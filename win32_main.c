// ======================================================================================
// win32_main.c - v0.0.1 - win32 platform main executable
// Author: Ngoc Nguyen
// ======================================================================================

#include "defs.h"
#include <windows.h>

#define INTRINS_IMPLEMENTATION
#include "intrins.h"

#define USE_LOG
#define os_debug_print win32_debug_print
#include "utils.h"

#define IKR_ASSERT_IMPLEMENTATION
#include "ikr_assert.h"
#define STB_SPRINTF_IMPLEMENTATION
#include "./stb_sprintf.h"

#define INPUT_IMPLEMENTATION
#include "input.h"

#define IKR_MEM_IMPLEMENTAION
#include "ikr_memory.h"

#include "win32.c"

#if !RELEASE
#define BASE_ADDR (LPVOID)(TERABYTE(2))
#else
#define BASE_ADDR 0
#endif

#define WIN32_PLATFORM_MEM_SIZE MEGABYTE(128)

#if !RELEASE
#define APP_DLL_NAME "builds\\debug\\render_app.dll"
#else
#define APP_DLL_NAME "render_app.dll"
#endif

GLOBAL b32 RUNNING = 0;

typedef struct Win32Dim { i32 width, height; } Win32Dim;


typedef struct Win32BackBuffer {
    void* mem;
    i32 width, height, pitch, bytes_per_pixel;
    BITMAPINFO bitmap_info;
} Win32BackBuffer;

GLOBAL Win32BackBuffer BACKBUFFER = {0};

INTERNAL KeyCode win32_vk_to_keycode(WPARAM vk) {
    KeyCode kc = KEY_MAX;
    switch (vk) {
    case VK_BACK:    kc = KEY_BS;    break;
    case VK_TAB:     kc = KEY_TAB;   break;
    case VK_RETURN:  kc = KEY_RET;   break;

    case VK_SHIFT:   kc = KEY_SHFT;  break;
    case VK_CONTROL: kc = KEY_CTRL;  break;
    case VK_MENU:    kc = KEY_ALT;   break;
    case VK_ESCAPE:  kc = KEY_ESC;   break;
    case VK_SPACE:   kc = KEY_SPC;   break;
    case VK_HOME:    kc = KEY_HOME;  break;
    case VK_END:     kc = KEY_END;   break;

    case VK_LEFT:    kc = KEY_LEFT;  break;
    case VK_RIGHT:   kc = KEY_RIGHT; break;
    case VK_UP:      kc = KEY_UP;    break;
    case VK_DOWN:    kc = KEY_DOWN;  break;

    case 0x30: kc = KEY_0; break;
    case 0x31: kc = KEY_1; break;
    case 0x32: kc = KEY_2; break;
    case 0x33: kc = KEY_3; break;
    case 0x34: kc = KEY_4; break;
    case 0x35: kc = KEY_5; break;
    case 0x36: kc = KEY_6; break;
    case 0x37: kc = KEY_7; break;
    case 0x38: kc = KEY_8; break;
    case 0x39: kc = KEY_9; break;

    case 0x41: kc = KEY_A; break;
    case 0x42: kc = KEY_B; break;
    case 0x43: kc = KEY_C; break;
    case 0x44: kc = KEY_D; break;
    case 0x45: kc = KEY_E; break;
    case 0x46: kc = KEY_F; break;
    case 0x47: kc = KEY_G; break;
    case 0x48: kc = KEY_H; break;
    case 0x49: kc = KEY_I; break;
    case 0x4a: kc = KEY_J; break;
    case 0x4b: kc = KEY_K; break;
    case 0x4c: kc = KEY_L; break;
    case 0x4d: kc = KEY_M; break;
    case 0x4e: kc = KEY_N; break;
    case 0x4f: kc = KEY_O; break;
    case 0x50: kc = KEY_P; break;
    case 0x51: kc = KEY_Q; break;
    case 0x52: kc = KEY_R; break;
    case 0x53: kc = KEY_S; break;
    case 0x54: kc = KEY_T; break;
    case 0x55: kc = KEY_U; break;
    case 0x56: kc = KEY_V; break;
    case 0x57: kc = KEY_W; break;
    case 0x58: kc = KEY_X; break;
    case 0x59: kc = KEY_Y; break;
    case 0x5a: kc = KEY_Z; break;

    // NOTE: only works for US layout
    case VK_OEM_1:      kc = KEY_SEMICOLON; break;
    case VK_OEM_PLUS:   kc = KEY_EQ;        break;
    case VK_OEM_COMMA:  kc = KEY_COMMA;     break;
    case VK_OEM_MINUS:  kc = KEY_MINUS;     break;
    case VK_OEM_PERIOD: kc = KEY_PERIOD;    break;
    case VK_OEM_2:      kc = KEY_FSLASH;    break;
    case VK_OEM_3:      kc = KEY_BACKTICK;  break;
    case VK_OEM_4:      kc = KEY_LBRACK;    break;
    case VK_OEM_5:      kc = KEY_BSLASH;    break;
    case VK_OEM_6:      kc = KEY_RBRACK;    break;
    case VK_OEM_7:      kc = KEY_QUOTE;     break;
    }

    return kc;
}

INTERNAL PerfCounter win32_perf_counter() {
    PerfCounter pc = {0};
    LARGE_INTEGER freq, start;
    i64 ticks_per_sec;
    QueryPerformanceFrequency((LARGE_INTEGER*)(&ticks_per_sec));
    pc.ticks_per_sec = (f64)ticks_per_sec;
    return pc;
}

INTERNAL void win32_perf_counter_start(PerfCounter* pc) {
    QueryPerformanceCounter((LARGE_INTEGER*)(&(pc->start)));
}

INTERNAL f32 win32_perf_counter_elapsed_ms(PerfCounter *pc) {
    i64 end;
    QueryPerformanceCounter((LARGE_INTEGER*)(&end));
    f64 ticks = (f64)((end - pc->start) * 1000);
    return (f32)(ticks / pc->ticks_per_sec);
}

INTERNAL inline Win32Dim win32_rect_dim(RECT rect) {
    Win32Dim dim = {rect.right - rect.left, rect.bottom - rect.top};
    return dim;
}

INTERNAL Win32Dim win32_adjusted_window_dim(i32 body_w, i32 body_h, DWORD style) {
    RECT dummy = { .left = 0, .top = 0, .right = body_w, .bottom = body_h };
    IKR_ASSERT(AdjustWindowRectEx(&dummy, style, true, 0));
    Win32Dim res = { dummy.right - dummy.left, dummy.bottom - dummy.top };
    return res;
}

INTERNAL Win32Dim win32_window_dim(HWND window) {
    RECT rect;
    GetClientRect(window, &rect);
    return win32_rect_dim(rect);
}

INTERNAL u32 win32_monitor_refresh_rate(HWND window) {
    // NOTE: if the window isn't on any monitor, this returns the
    // refresh rate of the primary monitor.
    HMONITOR monitor = MonitorFromWindow(window, MONITOR_DEFAULTTOPRIMARY);
    // This is just to get the monitor's name
    MONITORINFOEX monitor_info = { .cbSize = sizeof(monitor_info) };
    GetMonitorInfo(monitor, (MONITORINFO*)&monitor_info);

    // This is actually the monitor info, fantastic api design as usual
    DEVMODE device_info = { .dmSize = sizeof(device_info) };
    if (!EnumDisplaySettings(monitor_info.szDevice, ENUM_CURRENT_SETTINGS, &device_info)) {
        u32 err = GetLastError();
        LOGF("Error: cannot get refresh rate, code: %u\n", err);
        return 0;
    }

    // This means that the refresh rate is the "default", whatever the fuck that means.
    // In any case, it's useless to us, so let's just treat this as a failure.
    if (device_info.dmDisplayFrequency == 1) {
        LOG("Error: cannot get refresh rate, EnumDisplaySettings returns 1\n");
        return 0;
    }

    return (u32)device_info.dmDisplayFrequency;
}

INTERNAL void win32_init_backbuffer(Win32BackBuffer *buf, i32 request_w, i32 request_h, ikrmem_Arena *arena) {
    if (buf->mem) VirtualFree(buf->mem, 0, MEM_RELEASE);

    buf->width = request_w;
    buf->height = request_h;
    buf->bytes_per_pixel = 4;

    buf->bitmap_info.bmiHeader.biSize        = sizeof(buf->bitmap_info.bmiHeader);
    buf->bitmap_info.bmiHeader.biWidth       = buf->width;
    buf->bitmap_info.bmiHeader.biHeight      = buf->height;
    buf->bitmap_info.bmiHeader.biPlanes      = 1;
    buf->bitmap_info.bmiHeader.biBitCount    = 32;
    buf->bitmap_info.bmiHeader.biCompression = BI_RGB;

    usize buf_size = buf->width * buf->height * buf->bytes_per_pixel;
    buf->mem = IKR_MEM_NEW_ITEMS(arena, u8, buf_size);
    IKR_ASSERT(buf->mem);
    buf->pitch = buf->width * buf->bytes_per_pixel;
}

INTERNAL AppCode win32_load_app(char *dll_path) {
    AppCode dll = {0};
    char tmp_dll_path[1024];
    stbsp_snprintf(tmp_dll_path, sizeof(tmp_dll_path), "%s_tmp.dll", dll_path);
    CopyFile(dll_path, tmp_dll_path, FALSE);

    HMODULE maybe_dll = LoadLibrary(tmp_dll_path);
    if (maybe_dll) {
        dll.module = (void*)maybe_dll;
        dll.update = (app_update_proc*)(GetProcAddress(maybe_dll, "update"));
        dll.init   = (app_init_proc*)(GetProcAddress(maybe_dll, "init"));
    }
    return dll;
}

INTERNAL b32 win32_unload_app(AppCode *app) {
    b32 res = 0;
    if (app->module) res = FreeLibrary((HMODULE)(app->module));

    app->init   = NULL;
    app->update = NULL;
    return res;
}

INTERNAL u64 win32_file_last_write_time(char *file_path) {
    WIN32_FIND_DATA find_data = {0};
    if (FindFirstFile(file_path, &find_data) == INVALID_HANDLE_VALUE) {
        LOGF("FindFirstFile failed for file %s with error %d\n", file_path, GetLastError());
        return 0;
    }
    ULARGE_INTEGER res = {0};
    res.LowPart  = find_data.ftLastWriteTime.dwLowDateTime;
    res.HighPart = find_data.ftLastWriteTime.dwHighDateTime;
    return res.QuadPart;
}

INTERNAL void win32_display_backbuffer(
    HDC device_ctx,
    i32 window_w, i32 window_h,
    Win32BackBuffer *buf
) {

    i32 display_w = buf->width;
    i32 display_h = buf->height;
    i32 x0 = 0;
    i32 y0 = 0;

    //black bars
    PatBlt(device_ctx, 0, 0, window_w, y0, BLACKNESS);
    PatBlt(device_ctx, 0, 0, x0, window_h, BLACKNESS);
    PatBlt(device_ctx,
           x0 + display_w, 0,
           window_w - x0 + display_h, window_h,
           BLACKNESS);
    PatBlt(device_ctx,
           0, y0 + display_h,
           window_w, window_h - y0 + display_h,
           BLACKNESS);

    StretchDIBits(
        device_ctx,
        x0, y0, display_w, display_h,
        0, 0, buf->width, buf->height,
        buf->mem, &(buf->bitmap_info),
        DIB_RGB_COLORS, SRCCOPY
    );
}

INTERNAL void win32_mouse_pos(MSG *msg, i32 *x, i32 *y, i32 *x_raw, i32 *y_raw) {
    *x_raw = (i32)(i16)(msg->lParam);
    *y_raw = ((i32)msg->lParam) >> 16;
    *x = *x_raw;
    *y = BACKBUFFER.height - *y_raw - 1;
}

INTERNAL void win32_proc_message(
    MSG *msg,
    HWND window,
    UserInput *user_input
) {
    switch(msg->message) {
    default: {
        TranslateMessage(msg);
        DispatchMessage(msg);
    } break;

    case WM_QUIT: RUNNING = false; break;

    case WM_KEYDOWN:
    case WM_KEYUP: {
        KeyCode translated_keycode = win32_vk_to_keycode(msg->wParam);
        if (translated_keycode == KEY_MAX) break;   // some keys we don't handle

        b32 was_down = (msg->lParam & (1 << 30)) != 0;
        b32 is_down  = (msg->lParam & (1 << 31)) == 0;
        ButtonState *key_state = &user_input->keyboard.key_states[translated_keycode];
        key_state->ended_down = (b8)is_down;
        if (was_down != is_down) key_state->half_transition_cnt += 1;
        if (is_down) input_add_keycode_to_queue(user_input, translated_keycode);
    } break;

    case WM_MOUSEMOVE: {
        i32 x, y, x_raw, y_raw;
        win32_mouse_pos(msg, &x, &y, &x_raw, &y_raw);
        input_set_mouse_pos(&user_input->mouse, x, y, x_raw, y_raw);
    } break;

    case WM_LBUTTONDOWN: {
        i32 x, y, x_raw, y_raw;
        win32_mouse_pos(msg, &x, &y, &x_raw, &y_raw);
        input_set_mouse_down(&user_input->mouse, x, y, x_raw, y_raw, MOUSE_BUTTON_LEFT);
    } break;

    case WM_LBUTTONUP: {
        i32 x, y, x_raw, y_raw;
        win32_mouse_pos(msg, &x, &y, &x_raw, &y_raw);
        input_set_mouse_up(&user_input->mouse, x, y, x_raw, y_raw, MOUSE_BUTTON_LEFT);
    } break;

    case WM_RBUTTONDOWN: {
        i32 x, y, x_raw, y_raw;
        win32_mouse_pos(msg, &x, &y, &x_raw, &y_raw);
        input_set_mouse_down(&user_input->mouse, x, y, x_raw, y_raw, MOUSE_BUTTON_RIGHT);
    } break;

    case WM_RBUTTONUP: {
        i32 x, y, x_raw, y_raw;
        win32_mouse_pos(msg, &x, &y, &x_raw, &y_raw);
        input_set_mouse_up(&user_input->mouse, x, y, x_raw, y_raw, MOUSE_BUTTON_RIGHT);
    } break;

    case WM_MOUSEWHEEL: {
    } break;

    }

}

LRESULT CALLBACK main_win_callback(HWND window, UINT msg, WPARAM wparam, LPARAM lparam) {
    LRESULT res = 0;
    switch (msg) {
    case WM_SIZE: {
    } break;

    case WM_SETCURSOR: {
        HCURSOR cursor = WIN32_STATE.show_cursor ? WIN32_STATE.cursor : NULL;
        SetCursor(cursor);
    } break;

    case WM_DESTROY: {
        RUNNING = false;
    } break;

    case WM_CLOSE: {
        RUNNING = false;
    } break;

    case WM_ACTIVATEAPP:{
    } break;

    case WM_PAINT: {
        PAINTSTRUCT paint = {0};
        HDC ctx = BeginPaint(window, &paint);
        Win32Dim dim = win32_window_dim(window);

        win32_display_backbuffer(ctx, dim.width, dim.height, &BACKBUFFER);
        EndPaint(window, &paint);
    } break;

    default: {
        res = DefWindowProc(window, msg, wparam, lparam);
    } break;
    }
    return res;
}

INT WINAPI WinMain(
    HINSTANCE instance,
    HINSTANCE prev_instance,
    PSTR cmdline,
    INT show_window
) {
    // load app code
    AppCode app = win32_load_app(APP_DLL_NAME);
    IKR_ASSERT(app.module);
    IKR_ASSERT(app.init && app.update);
    u64 app_dll_lwt = win32_file_last_write_time(APP_DLL_NAME);
    IKR_ASSERT(app_dll_lwt);

    // initialize performance counter
    PerfCounter perf_counter = win32_perf_counter();
    IKR_ASSERT(perf_counter.ticks_per_sec > 0);
    IKR_ASSERT(timeBeginPeriod(1) == TIMERR_NOERROR);

    // memory initialization

    ikrmem_Arena win32_arena = {0};
    // TODO: look at how virtual alloc works more closely so we don't get more pages
    //       than we need for no reason.
    u64 total_mem_size = APP_MEM_SIZE + WIN32_PLATFORM_MEM_SIZE;
    void* total_mem = win32_valloc(BASE_ADDR, total_mem_size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    win32_arena.base = total_mem;
    win32_arena.size = WIN32_PLATFORM_MEM_SIZE;

    IKR_ASSERT(total_mem != NULL);
    WIN32_STATE.mem = total_mem;
    WIN32_STATE.show_cursor = 1;
#if RELEASE
    WIN32_STATE.show_cursor = 0;
#endif
    WIN32_STATE.cursor = LoadCursor(NULL, IDC_ARROW);

    void *mem_for_app = (u8*)(total_mem) + WIN32_PLATFORM_MEM_SIZE;
    Memory app_mem = {0};
    app_mem.mem = mem_for_app;
    app_mem.mem_size = APP_MEM_SIZE;
    app_mem.debug_print                 = win32_debug_print;
    app_mem.read_file                   = win32_read_file;
    app_mem.read_asset_file             = win32_read_asset_file;
    app_mem.write_file                  = win32_write_file;
    app_mem.perf_counter                = &perf_counter;
    app_mem.perf_counter_start          = win32_perf_counter_start;
    app_mem.perf_counter_elapsed_ms     = win32_perf_counter_elapsed_ms;

    i32 display_w = (i32)GetSystemMetrics(SM_CXSCREEN);
    i32 display_h = (i32)GetSystemMetrics(SM_CYSCREEN);
    i32 requested_w, requested_h;

    if (!app.init(&app_mem, display_w, display_h, &requested_w, &requested_h)) {
        win32_debug_print("app initialization failed, exiting...\n");
        VirtualFree(total_mem, 0, MEM_RELEASE);
        return 1;
    }

    IKR_ASSERT(requested_w <= display_w);
    IKR_ASSERT(requested_h <= display_h);
    win32_init_backbuffer(&BACKBUFFER, requested_w, requested_h, &win32_arena);
    BackBuffer backbuffer = {0};
    backbuffer.pixels          = (u32*)BACKBUFFER.mem;
    backbuffer.width           = BACKBUFFER.width;
    backbuffer.height          = BACKBUFFER.height;


    WIN32_STATE.win_pos.length = sizeof(WINDOWPLACEMENT);
    WNDCLASS win_class = {0};
    win_class.style          = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
    win_class.lpfnWndProc    = main_win_callback;
    win_class.hInstance      = instance;
    win_class.lpszClassName  = "Toy Renderer";
    IKR_ASSERT(RegisterClass(&win_class));

    DWORD window_style = WS_VISIBLE | WS_OVERLAPPEDWINDOW;
    Win32Dim adjusted_dim = win32_adjusted_window_dim(requested_w, requested_h, window_style);

    HWND win_handle = CreateWindowEx(
        0, win_class.lpszClassName,
        "Toy Renderer Window",
        window_style,
        CW_USEDEFAULT, CW_USEDEFAULT,
        adjusted_dim.width, adjusted_dim.height,
        0, 0, instance, 0
    );
    IKR_ASSERT(win_handle);
    HDC device_ctx = GetDC(win_handle);

    Win32Dim win_dim = win32_window_dim(win_handle);
    IKR_ASSERT(requested_w <= win_dim.width);
    IKR_ASSERT(requested_h <= win_dim.height);

    i32 monitor_refresh_rate = win32_monitor_refresh_rate(win_handle);
    IKR_ASSERT(monitor_refresh_rate >= 30);
    f32 target_ms_per_frame = (f32)((0.001 * (f64)(monitor_refresh_rate)));
    f32 time_since_last_frame = 0.0f;

    // initialize user input
    input_init_keycode_lookup();
    UserInput user_input = {0};

    RUNNING = 1;
    while (RUNNING) {
        // check the dll write time and reload it if it's newer
        u64 new_app_dll_lwt = win32_file_last_write_time(APP_DLL_NAME);
        if (new_app_dll_lwt > app_dll_lwt) {
            app_dll_lwt = new_app_dll_lwt;
            IKR_ASSERT(win32_unload_app(&app));
            app = win32_load_app(APP_DLL_NAME);
            IKR_ASSERT(app.module && app.init && app.update);
        }

        input_begin(&user_input);
        MSG win32_msg;
        while (PeekMessage(&win32_msg, 0, 0, 0, PM_REMOVE)) {
            win32_proc_message(&win32_msg, win_handle, &user_input);
        }
        input_end(&user_input);

        win32_perf_counter_start(&perf_counter);
        f32 elapsed_ms = win32_perf_counter_elapsed_ms(&perf_counter);
        time_since_last_frame += elapsed_ms;

        RUNNING &= app.update(&app_mem, &backbuffer, &user_input, display_w, display_h, time_since_last_frame);
        DWORD sleep_ms = 0;
        if (elapsed_ms < target_ms_per_frame) {
            sleep_ms = (DWORD)(target_ms_per_frame - elapsed_ms);
            Sleep(sleep_ms);
        }

        elapsed_ms = win32_perf_counter_elapsed_ms(&perf_counter);
        time_since_last_frame = elapsed_ms;

        win32_display_backbuffer(device_ctx, display_w, display_h, &BACKBUFFER);
    }

    IKR_ASSERT(VirtualFree(total_mem, 0, MEM_RELEASE));
    return 0;
}
