#ifndef IKR_GRAPHICS_H
#define IKR_GRAPHICS_H
// needs ikr_math.h, defs.h, intrins.h;

#ifndef IKRG_BACKEND_CPU
#define IKRG_BACKEND_CPU 0
#endif

#ifndef IKRG_BACKEND_GL
#define IKRG_BACKEND_GL 0
#endif

#undef IKRG_BACKEND_CPU
#define IKRG_BACKEND_CPU 1

typedef struct ikrg_Font {
    stbtt_fontinfo font_info;
    char *name;
    f32 scale;
    int height, max_width;
    int baseline;
} ikrg_Font;

typedef struct ikrg_Color {
    union {
        struct { f32 b, g, r, a; };
        f32 bgra[4];
        f32 bgr[3];
    };
} ikrg_Color;

#pragma pack(push, 1)
typedef struct {u8 b, g, r, _; } ikrg_Pixel;
#pragma pack(pop)

#define IKRG_FONT_MAX_GLYPHS 256

#ifndef IKRG_FONT_GLYPH_MAX_HEIGHT
#define IKRG_FONT_GLYPH_MAX_HEIGHT 100
#endif

#ifndef IKRG_FONT_GLYPH_MAX_WIDTH
#define IKRG_FONT_GLYPH_MAX_WIDTH 100
#endif

#ifndef IKRG_FONT_GLYPH_CACHE_SIZE
#define IKRG_FONT_GLYPH_CACHE_SIZE 128
#endif

typedef struct ikrg_GlyphKey  { void *font_ptr; i32 font_height; char ch; } ikrg_GlyphKey;
typedef struct ikrg_GlyphBitmap {
    struct ikrg_GlyphBitmap *next, *prev;
    ikrg_GlyphKey key;
    i32 glyph_idx;
    i32 atlas_offset;
    int adv, lsb;
    int x0, y0, x1, y1;
    i32 stride;
    u8 *bitmap;
    usize bitmap_size;
} ikrg_GlyphBitmap;
typedef struct ikrg_GlyphList { ikrg_GlyphBitmap *head, *tail; } ikrg_GlyphList;

typedef struct ikrg_GlyphCache {
    ikrg_GlyphList table[IKRG_FONT_GLYPH_CACHE_SIZE + 1];
    i32 id_count;
    ikrg_GlyphBitmap glyph_bmps[IKRG_FONT_MAX_GLYPHS];
    u8 glyph_atlas[IKRG_FONT_MAX_GLYPHS * IKRG_FONT_GLYPH_MAX_HEIGHT * IKRG_FONT_GLYPH_MAX_WIDTH];
} ikrg_GlyphCache;

#define ikrg_MAX_COMMANDS 1024 * 256
typedef struct ikrg_Context {
    ikrg_Font *font;
    ikrg_GlyphCache glyph_cache;

    u8 cmd_list[ikrg_MAX_COMMANDS];
    i32 cmd_list_len;
    i32 dst_w, dst_h;
    u32 *dst;
} ikrg_Context;

typedef struct ikrg_Rect { v2f32 pos, dim; } ikrg_Rect;

typedef enum ikrg_CommandKind {
    IKRG_CMD_BASE,
    IKRG_CMD_TEXT,
    IKRG_CMD_RECT,
    IKRG_CMD_BMP,
} ikrg_CommandKind;

typedef struct ikrg_CommandBase   { ikrg_CommandKind kind; i32 size; } ikrg_CommandBase;
typedef struct ikrg_CommandRect   { ikrg_CommandBase base; ikrg_Rect rect; ikrg_Color color; } ikrg_CommandRect;
typedef struct ikrg_CommandText   { ikrg_CommandBase base; char* text; v2f32 pos; i32 len; ikrg_Color color; } ikrg_CommandText;
typedef struct ikrg_CommandBitmap { ikrg_CommandBase base; void* data; v2f32 pos; i32 width, height; } ikrg_CommandBitmap;

typedef union ikrg_Command {
    ikrg_CommandKind    kind;
    ikrg_CommandBase    base;
    ikrg_CommandRect    rect;
    ikrg_CommandText    text;
    ikrg_CommandBitmap  bmp;
} ikrg_Command;

void ikrg_set_font_height(ikrg_Font *font, i32 height);

bool ikrg_next_command(ikrg_Context *ctx, ikrg_Command **cmd_pp);
void ikrg_rect(ikrg_Context *ctx, ikrg_Rect rect, ikrg_Color color);
void ikrg_text(ikrg_Context *ctx, v2f32 pos, char *text, i32 len, ikrg_Color color);
void ikrg_bitmap(ikrg_Context *ctx, v2f32 pos, void *data, i32 width, i32 height);
void ikrg_begin(ikrg_Context *ctx, void *dst, i32 dst_w, i32 dst_h, ikrg_Font *font);
void ikrg_end(ikrg_Context *ctx);

inline ikrg_Color ikrg_color3(f32 r, f32 g, f32 b)        { return (ikrg_Color){b, g, r, 1}; }
inline ikrg_Color ikrg_color4(f32 r, f32 g, f32 b, f32 a) { return (ikrg_Color){b, g, r, a}; }
inline u32 ikrg_rgb_to_pixel(f32 r, f32 g, f32 b) {
    return ((ROUND_TO(u32, r)) << 16) | ((ROUND_TO(u32, g)) << 8 ) | ((ROUND_TO(u32, b)) << 0 );
}

#define IKRG_RECT(x, y, w, h) (ikrg_Rect){(v2f32){(x), (y)}, (v2f32){(w), (h)}}

#ifdef IKRG_SHORTHAND

typedef ikrg_Color Color;
typedef ikrg_Pixel Pixel;
typedef ikrg_Rect  rect;

#define color3 ikrg_color3
#define color4 ikrg_color4

#define RECT IKRG_RECT

#endif

#ifdef IKRG_STD_COLOR
#define WHITE    ikrg_color3(1, 1, 1)
#define BLACK    ikrg_color3(0, 0, 0)
#define GREY     ikrg_color3(0.5f, 0.5f, 0.5f)
#define RED      ikrg_color3(1, 0, 0)
#define GREEN    ikrg_color3(0, 1, 0)
#define BLUE     ikrg_color3(0, 0, 1)
#define CYAN     ikrg_color3(0, 1, 1)
#define VIOLET   ikrg_color3(1, 0, 1)
#define YELLOW   ikrg_color3(1, 1, 0)
#endif

#endif
