#include "win32.h"
// needs defs.h, intrins.h, stb_sprintf.h

#ifndef USE_CONSOLE
#define USE_CONSOLE 0
#endif

Win32State WIN32_STATE = {0};

HANDLE STDOUT = INVALID_HANDLE_VALUE;
HANDLE STDIN  = INVALID_HANDLE_VALUE;
HANDLE STDERR = INVALID_HANDLE_VALUE;

b32 win32_std_handles_init() {
    if (STDOUT == INVALID_HANDLE_VALUE) STDOUT = GetStdHandle(STD_OUTPUT_HANDLE);
    if (STDIN  == INVALID_HANDLE_VALUE) STDIN  = GetStdHandle(STD_INPUT_HANDLE);
    if (STDERR == INVALID_HANDLE_VALUE) STDERR = GetStdHandle(STD_ERROR_HANDLE);

    return STDOUT != INVALID_HANDLE_VALUE &&
           STDIN  != INVALID_HANDLE_VALUE &&
           STDERR != INVALID_HANDLE_VALUE;
}

void win32_debug_print(const char * fmt, ...) {
    char debug_str[DEBUG_PRINT_BUFSIZE];
    va_list args;
    va_start(args, fmt);
    int len = stbsp_vsnprintf(debug_str, DEBUG_PRINT_BUFSIZE, fmt, args);
    va_end(args);
#ifdef USE_CONSOLE
    DWORD written;
    WriteConsoleA(STDOUT, debug_str, len, &written, NULL);
#endif
    OutputDebugString(debug_str);
}

void* win32_valloc(void* base_addr, usize size, u32 alloc_type, u32 protect) {
    void* mem = VirtualAlloc(base_addr, size, alloc_type, protect);
    return mem;
}

b32 win32_read_file(char *filename, DataBuffer *buffer) {
    HANDLE file_handle = CreateFile(
        filename,
        GENERIC_READ, FILE_SHARE_READ,
        NULL, OPEN_EXISTING, 0, 0
    );
    if (file_handle == INVALID_HANDLE_VALUE) {
        return 0;
    }

    LARGE_INTEGER file_size;
    if (!GetFileSizeEx(file_handle, &file_size)) {
        return 0;
    }

    u32 file_size_trunc = (u32)file_size.QuadPart;
    buffer->data = win32_valloc(0, file_size_trunc, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    if (!(buffer->data)) {
        return 0;
    }
    u32 bytes_read;
    if (!ReadFile(file_handle, buffer->data, file_size_trunc, &bytes_read, 0)) {
        VirtualFree(buffer->data, 0, MEM_RELEASE);
        return 0;
    }
    DEBUG_ASSERT(file_size_trunc == bytes_read);
    buffer->size = file_size_trunc;

    return 1;
}

b32 win32_read_asset_file(char *filename, DataBuffer *buf) {
    char asset_path[512] = {0};
    int path_len = stbsp_snprintf(asset_path, 1024, "assets\\%s", filename);
    DEBUG_ASSERT(path_len != 0);
    return win32_read_file(asset_path, buf);
}

b32 win32_write_file(char *filename, DataBuffer *buf) {
    return 0;
}
