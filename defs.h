// ======================================================================================
// defs.h - v0.0.1 - general definitions, interfaces, typedefs, and macros
// Author: Ngoc Nguyen
// ======================================================================================

#ifndef DEFS_H
#define DEFS_H

#ifndef RELEASE
#define RELEASE 0
#endif

#ifndef COMPILER_MSVC
#define COMPILER_MSVC 0
#endif

#ifndef COMPILER_LLVM
#define COMPILER_LLVM 0
#endif

#ifndef COMPILER_GCC
#define COMPILER_GCC 0
#endif

#ifndef OS_WIN32
#define OS_WIN32 0
#endif

#ifndef OS_LINUX
#define OS_LINUX 0
#endif

#ifndef OS_DARWIN
#define OS_DARWIN 0
#endif

#if !COMPILER_MSVC && !COMPILER_LLVM
    #if _MSC_VER
        #undef COMPILER_MSVC
        #define COMPILER_MSVC 1
    #elif __clang__
        #undef COMPILER_LLVM
        #define COMPILER_LLVM 1
    #elif __GNUC__
        #undef COMPILER_GCC
        #define COMPILER_GCC 1
    #endif
#endif

#if !OS_WIN32 && !OS_LINUX && !OS_DARWIN
    #if _WIN32
        #undef OS_WIN32
        #define OS_WIN32 1
    #elif __linux__
        #undef OS_LINUX
        #define OS_LINUX 1
    #elif __APPLE__
        #undef OS_DARWIN
        #define OS_DARWIN 1
    #endif
#endif


#define GLOBAL static
#define INTERNAL static

#if COMPILER_MSVC || COMPILER_LLVM
#define LIB_EXPORT __declspec ( dllexport )
#else
#define LIB_EXPORT
#endif

#ifdef __cplusplus
#include <stdalign.h>
#else
#define alignof _Alignof
#endif

// TODO: endian specific types

typedef _Bool                   bool;
typedef unsigned char           byte;

typedef char                    i8;
typedef short                   i16;
typedef long                    i32;
typedef long long               i64;

typedef unsigned char           u8;
typedef unsigned short          u16;
typedef unsigned long           u32;
typedef unsigned long long      u64;

typedef unsigned char           b8;
typedef unsigned short          b16;
typedef unsigned long           b32;
typedef unsigned long long      b64;

typedef float                   f32;
typedef double                  f64;

// NOTE: no 32-bit pointer types for now
typedef size_t                  usize;
typedef unsigned long long      uintptr;
typedef signed long long        intptr;

#define true  1
#define false 0

// TODO: more min max constants
#define U64_MAX 18446744073709551615ULL
#define U32_MAX 4294967295
#define U16_MAX 65535
#define U8_MAX  255

#define I64_MAX 9223372036854775807LL
#define I32_MAX 2147483647
#define I16_MAX 32767
#define I8_MAX  127

#define I64_MIN (-9223372036854775807LL - 1)
#define I32_MIN (-2147483647 - 1)
#define I16_MIN (-32768)
#define I8_MIN  (-128)

#define KILOBYTE(val) ((val) * 1024LL)
#define MEGABYTE(val) ((val) * 1024LL * 1024LL)
#define GIGABYTE(val) ((val) * 1024LL * 1024LL * 1024LL)
#define TERABYTE(val) ((val) * 1024LL * 1024LL * 1024LL * 1024LL)

#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define CLAMP(v, lower, upper) (MAX((lower), MIN((v), (upper))))
#define ARRAY_LEN(a) ((sizeof(a)) / (sizeof((a)[0])))

#ifndef NULL
#define NULL 0
#endif

#define DEBUG_PRINT_BUFSIZE 512

#if COMPILER_MSVC || COMPILER_LLVM
#define FORCE_INLINE __forceinline
#endif

typedef struct DataBuffer { void *data; u32 size; } DataBuffer;

#define OS_DEBUG_PRINT_PROC(name) void name(const char* fmt, ...)
typedef OS_DEBUG_PRINT_PROC(os_debug_print_proc);

#define OS_READ_FILE_PROC(name) b32 name(char *filename, DataBuffer *buf)
typedef OS_READ_FILE_PROC(os_read_file_proc);

#define OS_WRITE_FILE_PROC(name) b32 name(char *filename, DataBuffer *buf)
typedef OS_WRITE_FILE_PROC(os_write_file_proc);

typedef struct PerfCounter {
    i64 start;
    f64 ticks_per_sec;
} PerfCounter;

#define OS_PERF_COUNTER_START_PROC(name) void name(PerfCounter *pc)
typedef OS_PERF_COUNTER_START_PROC(os_perf_counter_start_proc);

#define OS_PERF_COUNTER_ELAPSED_MS_PROC(name) f32 name(PerfCounter *pc)
typedef OS_PERF_COUNTER_ELAPSED_MS_PROC(os_perf_counter_elapsed_ms_proc);

#define _APP_PERM_MEM_SIZE MEGABYTE(512)
#define _APP_TEMP_MEM_SIZE GIGABYTE(1)
#define APP_MEM_SIZE _APP_PERM_MEM_SIZE + _APP_TEMP_MEM_SIZE;

typedef struct Memory {

    // os provided procedures
    os_debug_print_proc               *debug_print;
    os_read_file_proc                 *read_file;
    os_read_file_proc                 *read_asset_file;
    os_write_file_proc                *write_file;
    os_perf_counter_start_proc        *perf_counter_start;
    os_perf_counter_elapsed_ms_proc   *perf_counter_elapsed_ms;

    PerfCounter *perf_counter;
    void *mem;
    usize mem_size;
} Memory;

typedef struct BackBuffer {
    u32* pixels;
    i32  width, height;
} BackBuffer;

#define APP_INIT_PROC(name) b32 name(Memory *mem, i32 display_w, i32 display_h, i32 *requested_w, i32 *requeted_h)
typedef APP_INIT_PROC(app_init_proc);

#define APP_UPDATE_PROC(name) b32 name(Memory *mem, BackBuffer *backbuffer, void *user_input, i32 display_w, i32 display_h, f32 step_ms)
typedef APP_UPDATE_PROC(app_update_proc);

typedef struct AppCode {
    void *module;
    app_init_proc *init;
    app_update_proc *update;
} AppCode;

#endif
