// ======================================================================================
// ikr_graphics.c - v0.0.1 - Simple graphics library
// Author: Ngoc Nguyen
// This is supposed to be a simple graphics library, with a default CPU rendering
// backend. GPU backends will come when I know how to do it.
// ======================================================================================

#include "ikr_graphics.h"
// needs defs.h, ikr_math.h, ikr_assert.h, intrins.h

#define SWAP_IF_GT(T, a, b) if ((a) > (b)) {T t = (a); (a) = (b); (b) = t; }

#ifdef IKRG_FONT_STB_TRUETYPE

#endif

#if IKRG_BACKEND_CPU

#define IKRG_INTERNAL static

#define FNV1a_HASH_INIT 2166136261
#define FNV1a_PRIME 16777619


IKRG_INTERNAL u32 ikrg_glyph_id_from_key(ikrg_GlyphKey key) {
    u8 *key_bytes = (u8*)&key;
    u32 id = FNV1a_HASH_INIT;
    int size = sizeof(key);
    while(size--) {
        id ^= *key_bytes * FNV1a_PRIME;
        ++key_bytes;
    }
    return (id % IKRG_FONT_GLYPH_CACHE_SIZE) + 1;
}

IKRG_INTERNAL void ikrg_glyph_metrics(
    i32 glyph_idx,
    ikrg_Font *font,
    int *adv, int *lsb,
    int *x0, int *y0, int *x1, int *y1
) {
    stbtt_GetGlyphHMetrics(&font->font_info, glyph_idx, adv, lsb);
    stbtt_GetGlyphBitmapBox(&font->font_info, glyph_idx, font->scale, font->scale,
                            x0, y0, x1, y1);
}

IKRG_INTERNAL inline b32 ikrg_glyph_key_eq(ikrg_GlyphKey k0, ikrg_GlyphKey k1) {
    return k0.font_ptr == k1.font_ptr && k0.font_height == k1.font_height && k0.ch == k1.ch;
}

IKRG_INTERNAL inline b32 ikrg_char_is_whitespace_or_newline(char c) {
    return c == ' ' || c == '\r' || c == '\n' || c == '\t';
}

IKRG_INTERNAL ikrg_GlyphBitmap ikrg_glyph_get(ikrg_GlyphCache *cache, ikrg_Font* font, char ch) {
    ASSERT(font->height <= IKRG_FONT_GLYPH_MAX_HEIGHT && font->max_width <= IKRG_FONT_GLYPH_MAX_WIDTH);

    ikrg_GlyphKey key = {font, font->height, ch};
    u32 id = ikrg_glyph_id_from_key(key);
    ikrg_GlyphBitmap **head = &cache->table[id].head;
    ikrg_GlyphBitmap **tail = &cache->table[id].tail;
    ikrg_GlyphBitmap *cur = *head;

    if (!cur) {
        // empty hash slot, so add this new glyph to it
        ASSERT(cache->id_count < ARRAY_LEN(cache->glyph_bmps));
        i32 glyph_idx = stbtt_FindGlyphIndex(&font->font_info, ch);
        ASSERT(glyph_idx > 0);

        ikrg_GlyphBitmap *glyph = cache->glyph_bmps + cache->id_count;
        glyph->key = key;
        glyph->glyph_idx = glyph_idx;
        ikrg_glyph_metrics(glyph_idx, font,
                           &glyph->adv, &glyph->lsb,
                           &glyph->x0, &glyph->y0, &glyph->x1, &glyph->y1);
        int width  = glyph->x1 - glyph->x0;
        int height = glyph->y1 - glyph->y0;
        glyph->atlas_offset = (i32)(cache->id_count) * IKRG_FONT_GLYPH_MAX_WIDTH;
        glyph->stride = IKRG_FONT_MAX_GLYPHS * IKRG_FONT_GLYPH_MAX_WIDTH;
        i32 rem_bytes = glyph->stride - glyph->atlas_offset;
        ASSERT(width < rem_bytes);
        if (!ikrg_char_is_whitespace_or_newline(ch)) {
            stbtt_MakeGlyphBitmap(&font->font_info, cache->glyph_atlas + glyph->atlas_offset,
                                  width, height, glyph->stride, font->scale, font->scale, glyph_idx);
        }

        *head = glyph;
        *tail = glyph;
        cache->id_count++;
        return *glyph;
    }

    if (ikrg_glyph_key_eq(cur->key, key)) return *cur;

    while (cur) {
        if (ikrg_glyph_key_eq(cur->key, key)) {
            cur->prev->next = cur->next;
            if (cur->next) {
                cur->next->prev = cur->prev;
            }
            cur->next = *head;
            (*head)->prev = cur;
            *head = cur;
            return *cur;
        }
        cur = cur->next;
    }

    if (cache->id_count < (ARRAY_LEN(cache->glyph_bmps) - 1)) {
        // add this new glyph to the head of the list
        i32 glyph_idx = stbtt_FindGlyphIndex(&font->font_info, ch);
        ASSERT(glyph_idx > 0);

        ikrg_GlyphBitmap *glyph = cache->glyph_bmps + cache->id_count;
        glyph->key = key;
        glyph->glyph_idx = glyph_idx;
        ikrg_glyph_metrics(glyph_idx, font,
                           &glyph->adv, &glyph->lsb,
                           &glyph->x0, &glyph->y0, &glyph->x1, &glyph->y1);
        int width  = glyph->x1 - glyph->x0;
        int height = glyph->y1 - glyph->y0;
        glyph->atlas_offset = (i32)(cache->id_count) * IKRG_FONT_GLYPH_MAX_WIDTH;
        glyph->stride = IKRG_FONT_MAX_GLYPHS * IKRG_FONT_GLYPH_MAX_WIDTH;
        i32 rem_bytes = glyph->stride - glyph->atlas_offset;
        ASSERT(width < rem_bytes);
        if (!ikrg_char_is_whitespace_or_newline(ch)) {
            stbtt_MakeGlyphBitmap(&font->font_info, cache->glyph_atlas + glyph->atlas_offset,
                                  width, height, glyph->stride, font->scale, font->scale, glyph_idx);
        }

        glyph->next = *head;
        (*head)->prev = glyph;
        *head = glyph;
        cache->id_count++;
        return *glyph;
    }

    // evict the oldest item
    i32 glyph_idx = stbtt_FindGlyphIndex(&font->font_info, ch);
    ASSERT(glyph_idx > 0);

    ikrg_GlyphBitmap *glyph = *tail;
    glyph->key = key;
    glyph->glyph_idx = glyph_idx;
    ikrg_glyph_metrics(glyph_idx, font,
                       &glyph->adv, &glyph->lsb,
                       &glyph->x0, &glyph->y0, &glyph->x1, &glyph->y1);
    int width  = glyph->x1 - glyph->x0;
    int height = glyph->y1 - glyph->y0;
    glyph->stride = IKRG_FONT_MAX_GLYPHS * IKRG_FONT_GLYPH_MAX_WIDTH;
    i32 rem_bytes = glyph->stride - glyph->atlas_offset;
    ASSERT(width < rem_bytes);
    if (!ikrg_char_is_whitespace_or_newline(ch)) {
        stbtt_MakeGlyphBitmap(&font->font_info, cache->glyph_atlas + glyph->atlas_offset,
                              width, height, glyph->stride, font->scale, font->scale, glyph_idx);
    }
    *tail = glyph->prev;
    glyph->prev->next = NULL;
    glyph->next = *head;
    glyph->prev = NULL;
    (*head)->prev = glyph;
    *head = glyph;
    cache->id_count++;
    return *glyph;
}

IKRG_INTERNAL void ikrg_render_text(ikrg_Context *ctx, char *text, v2f32 pos, i32 text_len, ikrg_Color text_color) {
    ikrg_Font *font = ctx->font;
    if (!(font && &font->font_info)) return;

    i32 start_x = ROUND_TO(i32, pos.x);
    i32 start_y = ROUND_TO(i32, pos.y);
    i32 cur_x = start_x;
    i32 cur_y = start_y;
    u8 char_bitmap[40 * 40];

    for (char *c = text; c < text + text_len; ++c) {
        ikrg_GlyphBitmap glyph = ikrg_glyph_get(&ctx->glyph_cache, ctx->font, *c);
        i32 x_adv = ROUND_TO(i32, font->scale * (f32)glyph.adv);
        int w = glyph.x1 - glyph.x0;
        int h = glyph.y1 - glyph.y0;
        if (*c == ' ' || *c == '\r' || *c == '\t') {
            cur_x += x_adv;
            continue;
        }
        if (*c == '\n') {
            cur_y += font->height;
            cur_x = start_x;
            continue;
        }
        u8* glyph_atlas = ctx->glyph_cache.glyph_atlas;

        int dst_y = cur_y - glyph.y1;
        for (int y = h - 1; y >= 0; --y) {
            int dst_x = cur_x + glyph.x0;
            ikrg_Pixel *px = (ikrg_Pixel*)ctx->dst + (dst_y * ctx->dst_w + dst_x);
            for (int x = 0; x < w; ++x) {
                int src_idx = glyph.atlas_offset + (y * glyph.stride + x);
                f32 alpha = ((f32)(glyph_atlas[src_idx])) / 255.0f;
                f32 new_r = 255.0f * (text_color.r * alpha + (1 - alpha) * ((f32)(px->r) / 255.0f));
                f32 new_g = 255.0f * (text_color.g * alpha + (1 - alpha) * ((f32)(px->g) / 255.0f));
                f32 new_b = 255.0f * (text_color.b * alpha + (1 - alpha) * ((f32)(px->b) / 255.0f));
                *(u32*)px = ikrg_rgb_to_pixel(new_r, new_g, new_b);
                ++px;
                ++dst_x;
            }
            ++dst_y;
        }
        cur_x += x_adv;
    }
}

IKRG_INTERNAL void ikrg_render_bitmap(ikrg_Context *ctx, void *bmp_data, v2f32 pos, i32 width, i32 height) {
    if (width <= 0 || height <= 0) return;
    i32 x0 = ROUND_TO(i32, pos.x);
    i32 y0 = ROUND_TO(i32, pos.y);

    for (i32 y = 0; y < height; ++y) {
        ikrg_Color* bmp_px = ((ikrg_Color*)bmp_data) + y * width;
        ikrg_Pixel* dst_px = ((ikrg_Pixel*)ctx->dst) + y0 * width + x0;
        for (i32 x = 0; x < width; ++x) {
            f32 dst_alpha = 1 - bmp_px->a;
            f32 new_r = 255.0f * (bmp_px->r + dst_alpha * ((f32)(dst_px->r) / 255.0f));
            f32 new_g = 255.0f * (bmp_px->g + dst_alpha * ((f32)(dst_px->g) / 255.0f));
            f32 new_b = 255.0f * (bmp_px->b + dst_alpha * ((f32)(dst_px->b) / 255.0f));
            *(u32*)dst_px = ikrg_rgb_to_pixel(new_r, new_g, new_b);
            ++dst_px;
            ++bmp_px;
        }
        ++y0;
    }
}

IKRG_INTERNAL void ikrg_fill_pixel_row(u32 *row, u32 pixel, int count) {
    if (count == 0) return;
    *row = pixel;
    u32 *last = row + count;
    for (int i = 1; i < count; i *= 2) {
        u32 *dst = row + i;
        u32 *src = row;
        usize size = MAX(0, MIN(last - dst, dst - src));
        if (size > 0) mem_copy(dst, src, size * sizeof(u32));
    }

}

IKRG_INTERNAL void ikrg_render_rect(ikrg_Context *ctx, ikrg_Rect rect, ikrg_Color color) {
    v2f32 p0 = rect.pos;
    v2f32 p1 = add_v2(p0, rect.dim);
    SWAP_IF_GT(f32, p0.x, p1.x);
    SWAP_IF_GT(f32, p0.y, p1.y);

    i32 x0 = ROUND_TO(i32, p0.x);
    i32 x1 = ROUND_TO(i32, p1.x);
    i32 y0 = ROUND_TO(i32, p0.y);
    i32 y1 = ROUND_TO(i32, p1.y);

    if (color.a != 0 && color.a != 1) {
        f32 r = color.r * color.a;
        f32 g = color.g * color.a;
        f32 b = color.b * color.a;
        f32 dst_a = 1 - color.a;

        for (i32 y = y0; y < y1; ++y) {
            ikrg_Pixel *px = (ikrg_Pixel*)ctx->dst + y * ctx->dst_w + x0;
            for (i32 x = x0; x < x1; ++x) {
                f32 new_r = 255.0f * (r + dst_a * ((f32)(px->r) / 255.0f));
                f32 new_g = 255.0f * (g + dst_a * ((f32)(px->g) / 255.0f));
                f32 new_b = 255.0f * (b + dst_a * ((f32)(px->b) / 255.0f));
                *(u32*)px = ikrg_rgb_to_pixel(new_r, new_g, new_b);
                ++px;
            }
        }
    } else {
        u32 new_px = ikrg_rgb_to_pixel(color.r * 255.0f, color.g * 255.0f, color.b * 255.0f);
        for (i32 y = y0; y < y1; ++y) {
            u32 *row = ctx->dst + y * ctx->dst_w + x0;
            ikrg_fill_pixel_row(row, new_px, x1 - x0);
        }
    }
}

#endif


IKRG_INTERNAL ikrg_Command* ikrg_push_command(ikrg_Context *ctx, ikrg_CommandKind kind, i32 size) {
    ASSERT(ctx->cmd_list_len < sizeof(ctx->cmd_list));
    ikrg_Command *cmd = (ikrg_Command*)(ctx->cmd_list + ctx->cmd_list_len);
    cmd->base.kind = kind;
    cmd->base.size = size;
    ctx->cmd_list_len += size;
    return cmd;
}

bool ikrg_next_command(ikrg_Context *ctx, ikrg_Command **cmd_pp) {
    ikrg_Command *cmd_p = *cmd_pp;

    if (!cmd_p) {
        cmd_p = (ikrg_Command*)(&ctx->cmd_list);
    } else {
        cmd_p = (ikrg_Command*)((u8*)(cmd_p) + cmd_p->base.size);
    }
    ikrg_Command *last = (ikrg_Command*)(ctx->cmd_list + ctx->cmd_list_len);
    *cmd_pp = cmd_p;
    return cmd_p != last;
}

void ikrg_rect(ikrg_Context *ctx, ikrg_Rect rect, ikrg_Color color) {
    if (rect.dim.x == 0 || rect.dim.y == 0) return;
    ikrg_Command *cmd = ikrg_push_command(ctx, IKRG_CMD_RECT, sizeof(ikrg_CommandRect));
    cmd->rect.rect = rect;
    cmd->rect.color = color;
}

void ikrg_set_font_height(ikrg_Font *font, i32 height) {
    if (!(font && &font->font_info)) return;
    font->height = height;
    int ascent, descent, line_gap;
    font->scale = stbtt_ScaleForPixelHeight(&font->font_info, (f32)font->height);
    stbtt_GetFontVMetrics(&font->font_info, &ascent, &descent, &line_gap);
    int x0, y0, x1, y1;
    stbtt_GetFontBoundingBox(&font->font_info, &x0, &y0, &x1, &y1);
    font->max_width = ROUND_TO(int, ((f32)(x1 - x0)) * font->scale);
}

void ikrg_text(ikrg_Context *ctx, v2f32 pos, char *text, i32 len, ikrg_Color color) {
    if (len <= 0) return;
    ikrg_Command *cmd = ikrg_push_command(ctx, IKRG_CMD_TEXT, sizeof(ikrg_CommandText));
    cmd->text.text = text;
    cmd->text.pos = pos;
    cmd->text.len = len;
    cmd->text.color = color;
}

void ikrg_bitmap(ikrg_Context *ctx, v2f32 pos, void *data, i32 width, i32 height) {
    if (width == 0 || height == 0) return;
    if (!data) return;

    ikrg_Command *cmd = ikrg_push_command(ctx, IKRG_CMD_BMP, sizeof(ikrg_CommandBitmap));
    cmd->bmp.data = data;
    cmd->bmp.pos = pos;
    cmd->bmp.width = width;
    cmd->bmp.height = height;
}

void ikrg_begin(ikrg_Context *ctx, void *dst, i32 dst_w, i32 dst_h, ikrg_Font *font) {
    ctx->cmd_list_len = 0;
    ctx->dst = dst;
    ctx->dst_w = dst_w;
    ctx->dst_h = dst_h;
    ctx->font = font;
    zero_mem(ctx->dst, ctx->dst_w * ctx->dst_h * 4);
}

void ikrg_end(ikrg_Context *ctx) {
    ikrg_Command *cmd_ptr = 0;
    while (ikrg_next_command(ctx, &cmd_ptr)) {
        switch(cmd_ptr->kind) {
        case IKRG_CMD_BASE: ASSERT(0);

        case IKRG_CMD_RECT: {
            ikrg_render_rect(ctx, cmd_ptr->rect.rect, cmd_ptr->rect.color);
        } break;

        case IKRG_CMD_TEXT: {
            ikrg_render_text(ctx, cmd_ptr->text.text, cmd_ptr->text.pos, cmd_ptr->text.len, cmd_ptr->text.color);
        } break;

        case IKRG_CMD_BMP: {
            ikrg_render_bitmap(ctx, cmd_ptr->bmp.data, cmd_ptr->bmp.pos,
                               cmd_ptr->bmp.width, cmd_ptr->bmp.height);
        } break;

        }
    }
}
