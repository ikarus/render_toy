#ifndef UTILS_H
#define UTILS_H

// TODO: make this more organized
#if OS_WIN32
#define PATH_SEP '\\'
#else
#define PATH_SEP '/'
#endif

inline const char* try_shorten_path(const char* filepath) {
    const char *cur = filepath;
    const char *maybe_short_path = 0;
    while(*cur) {
        if (*cur == PATH_SEP) {
            maybe_short_path = cur;
        }
        ++cur;
    }

    if (maybe_short_path && maybe_short_path + 1 < cur) {
        return maybe_short_path + 1;
    }
    return filepath;
}

// takes a printf-like procedure, and add the file path, procedure name, and line number
// of the calling site to the output string
#define _LOG(print_func, fmt) print_func("%s | %s | %d: " fmt, __FILE__, __func__, __LINE__)
#define _LOG_SHORT_FILE(print_func, fmt) print_func("%s | %s | %d: " fmt, try_shorten_path(__FILE__), __func__, __LINE__)
#define _LOG_VA(print_func, fmt, ...) print_func("%s | %s | %d: " fmt, __FILE__, __func__, __LINE__, __VA_ARGS__)
#define _LOG_SHORT_FILE_VA(print_func, fmt, ...) print_func("%s | %s | %d: " fmt, try_shorten_path(__FILE__), __func__, __LINE__, __VA_ARGS__)

// wraps os print functions in LOG, use this for convenient logging,
// just as if you're calling printf. Needs "os.h", if you're not defining
// your own print functions.
#ifdef USE_LOG
    #ifndef LOG_FULL
        #define LOG_FULL(fmt) _LOG(os_debug_print, fmt)
    #endif

    #ifndef LOG
        #define LOG(fmt) _LOG_SHORT_FILE(os_debug_print, fmt)
    #endif

    #ifndef LOGF_FULL
        #define LOGF_FULL(fmt, ...) _LOG_VA(os_debug_print, fmt, __VA_ARGS__)
    #endif

    #ifndef LOGF
        #define LOGF(fmt, ...) _LOG_SHORT_FILE_VA(os_debug_print, fmt, __VA_ARGS__)
    #endif

#endif

// needs DEBUG_BREAK definition
#if RELEASE
    #define DEBUG_ASSERT (void)(0)
#else
    #ifdef USE_LOG
        #define DEBUG_ASSERT(expr) if (!(expr)) { LOG("assertion failed\n"); DEBUG_BREAK; }
    #else
        #define DEBUG_ASSERT(expr) if (!(expr)) { DEBUG_BREAK; }
    #endif
#endif

#define DEBUG_UNREACHABLE DEBUG_ASSERT(0)

#endif
