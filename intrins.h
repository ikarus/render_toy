// ======================================================================================
// intrins.h - v0.0.1 - platform and compiler intrinsics
// Author: Ngoc Nguyen
// ======================================================================================

#ifndef INTRINS_H
#define INTRINS_H

#include <math.h>
#include <memory.h>
#include <intrin.h>
// needs defs.h

#ifdef __cplusplus
#define INTRINS_PUBLIC extern "C"
#else
#define INTRINS_PUBLIC extern
#endif

#if COMPILER_MSVC
#pragma intrinsic(_BitScanForward)
#pragma intrinsic(_BitScanForward64)

#pragma intrinsic(memcpy)
#pragma intrinsic(memcmp)
#pragma intrinsic(memset)
#pragma intrinsic(powf)
#pragma intrinsic(_rotl)
#pragma intrinsic(_rotl64)
#pragma intrinsic(_rotr)
#pragma intrinsic(_rotr64)
#pragma intrinsic(__debugbreak)

#define DEBUG_BREAK __debugbreak()
#endif

typedef struct {
    u32 where;
    b8 found;
} BitscanResult;

#define ROUND_TO(itype, f) (itype)(round_f32((f)))

#define ABS(x) _Generic((x), \
    f32: abs_f32,\
    i32: abs_i32,\
    default: abs_i32\
    )(x)

INTRINS_PUBLIC inline b32 sign_f32(f32 val);
INTRINS_PUBLIC inline f32 round_f32(f32 val);
INTRINS_PUBLIC inline i32 round_f32_to_i32(f32 val);
INTRINS_PUBLIC inline i32 trunc_f32_to_i32(f32 val);
INTRINS_PUBLIC inline i32 floor_f32_to_i32(f32 val);
INTRINS_PUBLIC inline f32 sin_f32(f32 val);
INTRINS_PUBLIC inline f32 cos_f32(f32 val);
INTRINS_PUBLIC inline f32 atan_f32(f32 val);
INTRINS_PUBLIC inline f32 abs_f32(f32 val);
INTRINS_PUBLIC inline i32 abs_i32(i32 val);
INTRINS_PUBLIC inline f32 sqrt_f32(f32 val);
INTRINS_PUBLIC inline f32 inv_sqrt_f32(f32 val);
INTRINS_PUBLIC inline f32 sq_f32(f32 val);
INTRINS_PUBLIC inline f32 pow_f32(f32 v, f32 e);
INTRINS_PUBLIC inline u32 bswap_u32(u32 val);
INTRINS_PUBLIC inline u32 rotate_left(u32 val, int shift);
INTRINS_PUBLIC inline u32 rotate_right(u32 val, int shift);
INTRINS_PUBLIC inline void* mem_copy(void* dst, const void* src, usize count);
INTRINS_PUBLIC inline void* mem_move(void* dst, const void* src, usize count);
INTRINS_PUBLIC inline int mem_compare(const void* b1, const void* b2, usize count);
INTRINS_PUBLIC inline void zero_mem(void* ptr, usize size);
INTRINS_PUBLIC inline void mem_set(void* ptr, int val, usize size);
INTRINS_PUBLIC inline BitscanResult lssb(u32 mask);
INTRINS_PUBLIC inline BitscanResult lssb64(u64 mask);

#endif // INTRINS_H

#ifdef INTRINS_IMPLEMENTATION

INTRINS_PUBLIC inline b32 sign_f32(f32 val) {
    return (b32)signbit(val);
}

INTRINS_PUBLIC inline f32 round_f32(f32 val) {
    return roundf(val);
}

INTRINS_PUBLIC inline i32 round_f32_to_i32(f32 val) {
    return (i32) roundf(val);
}

INTRINS_PUBLIC inline i32 trunc_f32_to_i32(f32 val) {
    return (i32)val;
}

INTRINS_PUBLIC inline i32 floor_f32_to_i32(f32 val) {
    return (i32)floorf(val);
}

INTRINS_PUBLIC inline f32 sin_f32(f32 val) {
    return sinf(val);
}

INTRINS_PUBLIC inline f32 cos_f32(f32 val) {
    return cosf(val);
}

INTRINS_PUBLIC inline f32 atan_f32(f32 val) {
    return atanf(val);
}

INTRINS_PUBLIC inline f32 abs_f32(f32 val) {
    return fabsf(val);
}

INTRINS_PUBLIC inline i32 abs_i32(i32 val) {
    return abs(val);
}

INTRINS_PUBLIC inline f32 sqrt_f32(f32 val) {
    return sqrtf(val);
}

INTRINS_PUBLIC inline f32 inv_sqrt_f32(f32 val) {
    return 1 / sqrtf(val);
}

INTRINS_PUBLIC inline f32 sq_f32(f32 val) {
    return val * val;
}

INTRINS_PUBLIC inline f32 pow_f32(f32 v, f32 e) {
    return powf(v, e);
}

// TODO: check if compiler generates bswap in optimized build
INTRINS_PUBLIC inline u32 bswap_u32(u32 val) {
    u32 res = 0;

    res |= ((val & 0x000000ff) << 24);
    res |= ((val & 0x0000ff00) << 8);
    res |= ((val & 0x00ff0000) >> 8);
    res |= ((val & 0xff000000) >> 24);
    return res;
}

INTRINS_PUBLIC inline u32 rotate_left(u32 val, int shift) {
    return _rotl(val, shift);
}

INTRINS_PUBLIC inline u32 rotate_right(u32 val, int shift) {
    return _rotr(val, shift);
}

INTRINS_PUBLIC inline void* mem_copy(void* dst, const void* src, usize count) {
    return memcpy(dst, src, count);
}

INTRINS_PUBLIC inline void* mem_move(void* dst, const void* src, usize count) {
    return memmove(dst, src, count);
}

INTRINS_PUBLIC inline int mem_compare(const void* b1, const void* b2, usize count) {
    return memcmp(b1, b2, count);
}

INTRINS_PUBLIC inline void zero_mem(void* ptr, usize size) {
    memset(ptr, 0, size);
}

INTRINS_PUBLIC inline void mem_set(void* ptr, int val, usize size) {
    memset(ptr, val, size);
}


// find the least significant set bit, returns 0
// if mask is zero, non zero otherwise
INTRINS_PUBLIC inline BitscanResult lssb(u32 mask) {
    BitscanResult res = {0};
#if COMPILER_MSVC
    res.found = _BitScanForward(&res.where, mask);
#endif
    return res;
}

INTRINS_PUBLIC inline BitscanResult lssb64(u64 mask) {
    BitscanResult res = {0};
#if COMPILER_MSVC
    res.found = _BitScanForward64(&res.where, mask);
#endif
    return res;
}

#endif
