#include "defs.h"

#define IKR_ASSERT_IMPLEMENTATION
#define STB_SPRINTF_IMPLEMENTATION
#include "ikr_assert.h"

#define ASSERT IKR_ASSERT

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#define INTRINS_IMPLEMENTATION
#include "intrins.h"

#define IKR_MATH_IMPLEMENTATION
#define IKRM_SHORTHAND
#include "ikr_math.h"

#define IKR_MEM_IMPLEMENTAION
#include "ikr_memory.h"

#define INPUT_IMPLEMENTATION
#include "input.h"

#define IKRG_STD_COLOR
#include "ikr_graphics.c"

#define BACKBUFFER_W 1080
#define BACKBUFFER_H 720
#define SMALL_BACKBUFFER_W 720
#define SMALL_BACKBUFFER_H 480

typedef struct Font {
    stbtt_fontinfo stb_font;
    char *name;
    f32 scale;
    int height, max_width;
    int ascent, descent, line_gap, baseline;
} Font;

typedef struct MemoryArena {
    u8* base;
    usize size, used;
} MemoryArena;

typedef struct ScratchBuffer {
    u8* base;
    usize size, used;
} ScratchBuffer;

typedef struct State {
    ikrmem_Arena   allocator;
    ikrmem_Scratch temp_allocator;

    ikrg_Font font;
    ikrg_Context *render_ctx;
    i32 app_width, app_height;

} State;

INTERNAL void load_and_init_font(char *name, ikrg_Font *font, int font_height, os_read_file_proc *read_file) {
    DataBuffer raw_font = {0};
    font->height = font_height;
    font->name = name;
    ASSERT(read_file(name, &raw_font));
    stbtt_InitFont(&font->font_info, raw_font.data,
                   stbtt_GetFontOffsetForIndex(raw_font.data, 0));
    ikrg_set_font_height(font, font_height);
}

INTERNAL usize cstring_len(const char *str) {
    if (!str) return 0;
    const char *cur = str;
    while(*cur) {
        ++cur;
    }

    return cur - str;
}

LIB_EXPORT b32 init(
    Memory *mem,
    i32 display_w,
    i32 display_h,
    i32 *requested_w,
    i32 *requested_h
) {
    if (display_w < SMALL_BACKBUFFER_W || display_h < SMALL_BACKBUFFER_H) {
        return 0;
    }

    if (display_w >= BACKBUFFER_W && display_h >= BACKBUFFER_H) {
        *requested_w = BACKBUFFER_W;
        *requested_h = BACKBUFFER_H;
    } else {
        *requested_w = SMALL_BACKBUFFER_W;
        *requested_h = SMALL_BACKBUFFER_H;
    }
    State *state = (State*)(mem->mem);
    state->app_width  = *requested_w;
    state->app_height = *requested_h;

    u8 *main_allocator_base = (u8*)(mem->mem) + sizeof(*state);
    usize main_mem_size = _APP_PERM_MEM_SIZE - sizeof(*state);
    ikrmem_arena_init(&state->allocator, main_allocator_base, main_mem_size);

    u8* temp_allocator_base = main_allocator_base + main_mem_size;
    usize temp_mem_size = mem->mem_size - (sizeof(*state) + main_mem_size);
    ikrmem_scratch_init(&state->temp_allocator, temp_allocator_base, temp_mem_size);

    load_and_init_font("Inconsolata.otf", &state->font, 50, mem->read_asset_file);
    input_init_keycode_lookup();

    state->render_ctx = IKR_MEM_NEW(&state->allocator, ikrg_Context);
    return 1;
}

static char test_string[1024] = {0};
static int test_string_len = 0;

LIB_EXPORT b32 update(
    Memory *mem,
    BackBuffer *backbuffer,
    void *user_input,
    i32 dislay_w, i32 display_h, f32 step_ms
) {
    // TODO: handle dynamic display width and height

    State *state = (State*)(mem->mem);
    UserInput *input = (UserInput*)user_input;

    i32 new_font_height = state->font.height;
    if (input_button_held(input->keyboard.key_states[KEY_CTRL]) &&
        input_button_tapped(input->keyboard.key_states[KEY_EQ])) {
        new_font_height += 5;
    }

    if (input_button_held(input->keyboard.key_states[KEY_CTRL]) &&
        input_button_tapped(input->keyboard.key_states[KEY_MINUS])) {
        new_font_height -= 5;
    }

    if (new_font_height != state->font.height) {
        ikrg_set_font_height(&state->font, new_font_height);
    }

    if (!input_button_held(input->keyboard.key_states[KEY_CTRL])) {
        for (u32 i = 0; i < input->keyboard.key_queue_end; ++i) {
            KeyCode kc = input->keyboard.key_queue[i];
            if (kc == KEY_BS) {
                test_string_len = MAX(0, test_string_len - 1);
            } else if (input_keycode_is_ascii(kc)) {
                char c = input_keycode_to_ascii_check_shift(input->keyboard, kc);
                if ((' ' <= c && c <= '~') && test_string_len < ARRAY_LEN(test_string)) {
                    test_string[test_string_len] = c;
                    test_string_len++;
                }
            }
        }
    }

    int start_dst_x = 150;
    int start_dst_y = 150;

    ikrg_begin(state->render_ctx, backbuffer->pixels, state->app_width, state->app_height, &state->font);
    ikrg_rect(state->render_ctx, IKRG_RECT(150, 250, 200, 100), RED);
    ikrg_text(state->render_ctx, v2(250, 350), test_string, (i32)test_string_len, GREY);
    ikrg_end(state->render_ctx);

    return 1;
}
