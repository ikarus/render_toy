// ======================================================================================
// input.h - v0.0.1 - keyboard and mouse input
// Author: Ngoc Nguyen
// ======================================================================================


#ifndef INPUT_H
#define INPUT_H

#ifdef __cplusplus
#define INPUT_PUBLIC extern "c"
#else
#define INPUT_PUBLIC extern
#endif

typedef enum KeyCode {

    // keys with ascii characters
    KEY_SPC,
    KEY_0,
    KEY_1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8,
    KEY_9,

    KEY_A,
    KEY_B,
    KEY_C,
    KEY_D,
    KEY_E,
    KEY_F,
    KEY_G,
    KEY_H,
    KEY_I,
    KEY_J,
    KEY_K,
    KEY_L,
    KEY_M,
    KEY_N,
    KEY_O,
    KEY_P,
    KEY_Q,
    KEY_R,
    KEY_S,
    KEY_T,
    KEY_U,
    KEY_V,
    KEY_W,
    KEY_X,
    KEY_Y,
    KEY_Z,

    KEY_MINUS,
    KEY_EQ,
    KEY_BSLASH,
    KEY_BACKTICK,
    KEY_LBRACK,
    KEY_RBRACK,
    KEY_SEMICOLON,
    KEY_QUOTE,
    KEY_COMMA,
    KEY_PERIOD,
    KEY_FSLASH,

    // modifiers and others
    KEY_BS,
    KEY_TAB,
    KEY_RET,

    KEY_LEFT,
    KEY_RIGHT,
    KEY_UP,
    KEY_DOWN,
    KEY_HOME,
    KEY_END,

    KEY_SHFT,
    KEY_CTRL,
    KEY_ALT,
    KEY_ESC,

    KEY_MAX,
} KeyCode;

#define KEYCODE_OFFSET (KEY_FSLASH + 1)
extern char KEYCODE_TO_ASCII[128] = {0};

typedef struct ButtonState {
    u8 half_transition_cnt;
    b8 ended_down;
} ButtonState;

typedef enum MouseButton {
    MOUSE_BUTTON_LEFT  = 1,
    MOUSE_BUTTON_MID   = 1 << 1,
    MOUSE_BUTTON_RIGHT = 1 << 2,
} MouseButton;

typedef u32 MouseBits;

typedef struct MouseInput {
    i32 x, y;
    i32 x_raw, y_raw;
    i32 x_last, y_last;
    i32 x_delta, y_delta;
    MouseBits down_bits;
    MouseBits up_bits;
    MouseBits pressed_bits;
    f32 wheel_revs;
} MouseInput;

typedef struct KeyboardInput {
    ButtonState key_states[128];
    KeyCode key_queue[256];
    u32 key_queue_end;
} KeyboardInput;

typedef struct UserInput {
    KeyboardInput keyboard;
    MouseInput mouse;
} UserInput;

INPUT_PUBLIC void input_init_keycode_lookup();
INPUT_PUBLIC char input_keycode_to_ascii(KeyCode kcode);
INPUT_PUBLIC char input_keycode_to_ascii_check_shift(KeyboardInput kb, KeyCode kcode);
INPUT_PUBLIC void input_set_mouse_pos(MouseInput *mouse, i32 x, i32 y, i32 x_raw, i32 y_raw);
INPUT_PUBLIC void input_set_mouse_down(MouseInput *mouse, i32 x, i32 y, i32 x_raw, i32 y_raw, MouseButton butt);
INPUT_PUBLIC void input_set_mouse_up(MouseInput *mouse, i32 x, i32 y, i32 x_raw, i32 y_raw, MouseButton butt);
INPUT_PUBLIC void input_add_keycode_to_queue(UserInput *input, KeyCode kcode);
INPUT_PUBLIC void input_begin(UserInput *input);
INPUT_PUBLIC void input_end(UserInput *input);

INPUT_PUBLIC inline b32 input_button_tapped(ButtonState b);
INPUT_PUBLIC inline b32 input_button_down(ButtonState b);
INPUT_PUBLIC inline b32 input_button_held(ButtonState b);
INPUT_PUBLIC inline b32 input_keycode_is_ascii(KeyCode kc);

#endif

#ifdef INPUT_IMPLEMENTATION

INPUT_PUBLIC void input_init_keycode_lookup() {
    // unshifted
    KEYCODE_TO_ASCII[KEY_SPC                       ] = ' ';
    KEYCODE_TO_ASCII[KEY_0                         ] = '0';
    KEYCODE_TO_ASCII[KEY_1                         ] = '1';
    KEYCODE_TO_ASCII[KEY_2                         ] = '2';
    KEYCODE_TO_ASCII[KEY_3                         ] = '3';
    KEYCODE_TO_ASCII[KEY_4                         ] = '4';
    KEYCODE_TO_ASCII[KEY_5                         ] = '5';
    KEYCODE_TO_ASCII[KEY_6                         ] = '6';
    KEYCODE_TO_ASCII[KEY_7                         ] = '7';
    KEYCODE_TO_ASCII[KEY_8                         ] = '8';
    KEYCODE_TO_ASCII[KEY_9                         ] = '9';
    KEYCODE_TO_ASCII[KEY_A                         ] = 'a';
    KEYCODE_TO_ASCII[KEY_B                         ] = 'b';
    KEYCODE_TO_ASCII[KEY_C                         ] = 'c';
    KEYCODE_TO_ASCII[KEY_D                         ] = 'd';
    KEYCODE_TO_ASCII[KEY_E                         ] = 'e';
    KEYCODE_TO_ASCII[KEY_F                         ] = 'f';
    KEYCODE_TO_ASCII[KEY_G                         ] = 'g';
    KEYCODE_TO_ASCII[KEY_H                         ] = 'h';
    KEYCODE_TO_ASCII[KEY_I                         ] = 'i';
    KEYCODE_TO_ASCII[KEY_J                         ] = 'j';
    KEYCODE_TO_ASCII[KEY_K                         ] = 'k';
    KEYCODE_TO_ASCII[KEY_L                         ] = 'l';
    KEYCODE_TO_ASCII[KEY_M                         ] = 'm';
    KEYCODE_TO_ASCII[KEY_N                         ] = 'n';
    KEYCODE_TO_ASCII[KEY_O                         ] = 'o';
    KEYCODE_TO_ASCII[KEY_P                         ] = 'p';
    KEYCODE_TO_ASCII[KEY_Q                         ] = 'q';
    KEYCODE_TO_ASCII[KEY_R                         ] = 'r';
    KEYCODE_TO_ASCII[KEY_S                         ] = 's';
    KEYCODE_TO_ASCII[KEY_T                         ] = 't';
    KEYCODE_TO_ASCII[KEY_U                         ] = 'u';
    KEYCODE_TO_ASCII[KEY_V                         ] = 'v';
    KEYCODE_TO_ASCII[KEY_W                         ] = 'w';
    KEYCODE_TO_ASCII[KEY_X                         ] = 'x';
    KEYCODE_TO_ASCII[KEY_Y                         ] = 'y';
    KEYCODE_TO_ASCII[KEY_Z                         ] = 'z';
    KEYCODE_TO_ASCII[KEY_MINUS                     ] = '-';
    KEYCODE_TO_ASCII[KEY_EQ                        ] = '=';
    KEYCODE_TO_ASCII[KEY_BSLASH                    ] = '\\';
    KEYCODE_TO_ASCII[KEY_BACKTICK                  ] = '`';
    KEYCODE_TO_ASCII[KEY_LBRACK                    ] = '[';
    KEYCODE_TO_ASCII[KEY_RBRACK                    ] = ']';
    KEYCODE_TO_ASCII[KEY_SEMICOLON                 ] = ';';
    KEYCODE_TO_ASCII[KEY_QUOTE                     ] = '\'';
    KEYCODE_TO_ASCII[KEY_COMMA                     ] = ',';
    KEYCODE_TO_ASCII[KEY_PERIOD                    ] = '.';
    KEYCODE_TO_ASCII[KEY_FSLASH                    ] = '/';

    // shifted
    KEYCODE_TO_ASCII[KEY_SPC       + KEYCODE_OFFSET] = ' ';
    KEYCODE_TO_ASCII[KEY_0         + KEYCODE_OFFSET] = ')';
    KEYCODE_TO_ASCII[KEY_1         + KEYCODE_OFFSET] = '!';
    KEYCODE_TO_ASCII[KEY_2         + KEYCODE_OFFSET] = '@';
    KEYCODE_TO_ASCII[KEY_3         + KEYCODE_OFFSET] = '#';
    KEYCODE_TO_ASCII[KEY_4         + KEYCODE_OFFSET] = '$';
    KEYCODE_TO_ASCII[KEY_5         + KEYCODE_OFFSET] = '%';
    KEYCODE_TO_ASCII[KEY_6         + KEYCODE_OFFSET] = '^';
    KEYCODE_TO_ASCII[KEY_7         + KEYCODE_OFFSET] = '&';
    KEYCODE_TO_ASCII[KEY_8         + KEYCODE_OFFSET] = '*';
    KEYCODE_TO_ASCII[KEY_9         + KEYCODE_OFFSET] = '(';
    KEYCODE_TO_ASCII[KEY_A         + KEYCODE_OFFSET] = 'A';
    KEYCODE_TO_ASCII[KEY_B         + KEYCODE_OFFSET] = 'B';
    KEYCODE_TO_ASCII[KEY_C         + KEYCODE_OFFSET] = 'C';
    KEYCODE_TO_ASCII[KEY_D         + KEYCODE_OFFSET] = 'D';
    KEYCODE_TO_ASCII[KEY_E         + KEYCODE_OFFSET] = 'E';
    KEYCODE_TO_ASCII[KEY_F         + KEYCODE_OFFSET] = 'F';
    KEYCODE_TO_ASCII[KEY_G         + KEYCODE_OFFSET] = 'G';
    KEYCODE_TO_ASCII[KEY_H         + KEYCODE_OFFSET] = 'H';
    KEYCODE_TO_ASCII[KEY_I         + KEYCODE_OFFSET] = 'I';
    KEYCODE_TO_ASCII[KEY_J         + KEYCODE_OFFSET] = 'J';
    KEYCODE_TO_ASCII[KEY_K         + KEYCODE_OFFSET] = 'K';
    KEYCODE_TO_ASCII[KEY_L         + KEYCODE_OFFSET] = 'L';
    KEYCODE_TO_ASCII[KEY_M         + KEYCODE_OFFSET] = 'M';
    KEYCODE_TO_ASCII[KEY_N         + KEYCODE_OFFSET] = 'N';
    KEYCODE_TO_ASCII[KEY_O         + KEYCODE_OFFSET] = 'O';
    KEYCODE_TO_ASCII[KEY_P         + KEYCODE_OFFSET] = 'P';
    KEYCODE_TO_ASCII[KEY_Q         + KEYCODE_OFFSET] = 'Q';
    KEYCODE_TO_ASCII[KEY_R         + KEYCODE_OFFSET] = 'R';
    KEYCODE_TO_ASCII[KEY_S         + KEYCODE_OFFSET] = 'S';
    KEYCODE_TO_ASCII[KEY_T         + KEYCODE_OFFSET] = 'T';
    KEYCODE_TO_ASCII[KEY_U         + KEYCODE_OFFSET] = 'U';
    KEYCODE_TO_ASCII[KEY_V         + KEYCODE_OFFSET] = 'V';
    KEYCODE_TO_ASCII[KEY_W         + KEYCODE_OFFSET] = 'W';
    KEYCODE_TO_ASCII[KEY_X         + KEYCODE_OFFSET] = 'X';
    KEYCODE_TO_ASCII[KEY_Y         + KEYCODE_OFFSET] = 'Y';
    KEYCODE_TO_ASCII[KEY_Z         + KEYCODE_OFFSET] = 'Z';
    KEYCODE_TO_ASCII[KEY_MINUS     + KEYCODE_OFFSET] = '_';
    KEYCODE_TO_ASCII[KEY_EQ        + KEYCODE_OFFSET] = '+';
    KEYCODE_TO_ASCII[KEY_BSLASH    + KEYCODE_OFFSET] = '|';
    KEYCODE_TO_ASCII[KEY_BACKTICK  + KEYCODE_OFFSET] = '~';
    KEYCODE_TO_ASCII[KEY_LBRACK    + KEYCODE_OFFSET] = '{';
    KEYCODE_TO_ASCII[KEY_RBRACK    + KEYCODE_OFFSET] = '}';
    KEYCODE_TO_ASCII[KEY_SEMICOLON + KEYCODE_OFFSET] = ':';
    KEYCODE_TO_ASCII[KEY_QUOTE     + KEYCODE_OFFSET] = '"';
    KEYCODE_TO_ASCII[KEY_COMMA     + KEYCODE_OFFSET] = '<';
    KEYCODE_TO_ASCII[KEY_PERIOD    + KEYCODE_OFFSET] = '>';
    KEYCODE_TO_ASCII[KEY_FSLASH    + KEYCODE_OFFSET] = '?';
}

INPUT_PUBLIC char input_keycode_to_ascii(KeyCode kcode) {
    return KEYCODE_TO_ASCII[kcode];
}

INPUT_PUBLIC char input_keycode_to_ascii_check_shift(KeyboardInput kb, KeyCode kcode) {
    KeyCode offset = (KeyCode)(input_button_held(kb.key_states[KEY_SHFT])) * KEYCODE_OFFSET;
    return KEYCODE_TO_ASCII[kcode + offset];
}

INPUT_PUBLIC void input_set_mouse_pos(MouseInput *mouse, i32 x, i32 y, i32 x_raw, i32 y_raw) {
    mouse->x = x;
    mouse->y = y;
    mouse->x_raw = x_raw;
    mouse->y_raw = y_raw;
}

INPUT_PUBLIC void input_set_mouse_down(
    MouseInput *mouse,
    i32 x, i32 y, i32 x_raw, i32 y_raw,
    MouseButton butt
) {
    input_set_mouse_pos(mouse, x, y, x_raw, y_raw);
    mouse->down_bits    |= butt;
    mouse->pressed_bits |= butt;
}

INPUT_PUBLIC void input_set_mouse_up(
    MouseInput *mouse,
    i32 x, i32 y, i32 x_raw, i32 y_raw,
    MouseButton butt
) {
    input_set_mouse_pos(mouse, x, y, x_raw, y_raw);
    mouse->up_bits    |= butt;
    mouse->down_bits  &= ~butt;
}

INPUT_PUBLIC inline b32 input_button_tapped(ButtonState b) {
    return b.ended_down && b.half_transition_cnt > 0;
}
INPUT_PUBLIC inline b32 input_button_down(ButtonState b) {
    return b.ended_down;
}
INPUT_PUBLIC inline b32 input_button_held(ButtonState b) {
    return b.ended_down && b.half_transition_cnt == 0;
}

INPUT_PUBLIC inline b32 input_keycode_is_ascii(KeyCode kc) {
    return KEY_SPC <= kc && kc <= KEY_FSLASH;
}

INPUT_PUBLIC void input_add_keycode_to_queue(UserInput *input, KeyCode kcode) {
    input->keyboard.key_queue[input->keyboard.key_queue_end] = kcode;
    input->keyboard.key_queue_end++;
}

INPUT_PUBLIC void input_begin(UserInput *input) {
    input->keyboard.key_queue_end = 0;
    for (int i = 0; i < ARRAY_LEN(input->keyboard.key_states); ++i) {
        input->keyboard.key_states[i].half_transition_cnt = 0;
    }
    input->mouse.pressed_bits = 0;
    input->mouse.up_bits = 0;
    input->mouse.x_last = input->mouse.x;
    input->mouse.y_last = input->mouse.y;
    input->mouse.x_delta = 0;
    input->mouse.y_delta = 0;
}

INPUT_PUBLIC void input_end(UserInput *input) {
    input->mouse.x_delta = input->mouse.x - input->mouse.x_last;
    input->mouse.y_delta = input->mouse.y - input->mouse.y_last;
}

#endif

