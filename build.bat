@echo OFF

SETLOCAL
set debug_build_path=builds\debug\
set warnings="/wd4189 /wd4211 /wd4201 /wd4100 /wd5105 /wd4996 /wd4101"
set libs="/link -incremental:no User32.lib Gdi32.lib Winmm.lib Xinput.lib"
set build_opts="-nologo -std:c11 -WX -W4 -FC -Zi"
set compile=cl

if not exist %debug_build_path%\ (
    echo "creating build directory %debug_build_path%"
    mkdir %debug_build_path%
)
cd %debug_build_path%

del *.exe > NUL 2>NUL
del *.dll > NUL 2>NULL
del *.pdb > NUL 2> NUL
del *.gs > NUL 2> NUL
del *.obj > NUL 2> NUL
del *.lib > NUL 2> NUL

echo building app as dll
%compile% "%build_opts%" "%warnings%" /Fd /Fe /LD /DLL ../../render_app.c /link -PDB:render_app_%dt_ext%.pdb -incremental:no

echo building win32_main
%compile% "%build_opts%" "%warnings%" /Fd /Fe ../../win32_main.c "%libs%"
