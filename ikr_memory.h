// ======================================================================================
// ikr_memory.h - v0.0.1 - Simple memory allocators and utilities
// Author: Ngoc Nguyen
// A simple memory arena and a scratch buffer allocators. TBA strings?
// ======================================================================================

#ifndef IKR_MEMORY_H
#define IKR_MEMORY_H

// ============================== Definitions ============================== //
#ifdef __cplusplus
#define IKR_MEM_PUBLIC extern "c"
#else
#define IKR_MEM_PUBLIC extern
#endif

#ifndef alignof
#include <stdalign.h>
#endif

#define IKR_MEM_INTERNAL static

#define IKR_MEM_SLICE_BODY(type) { type *ptr; usize len; }

typedef struct ikrmem_ByteSlice IKR_MEM_SLICE_BODY(unsigned char) ikrmem_ByteSlice;
typedef struct ikrmem_String    IKR_MEM_SLICE_BODY(char) ikrmem_String;

// TODO: add last alloc for resizing
typedef struct ikrmem_Arena {
    usize size;
    usize used;
    unsigned char *base;
} ikrmem_Arena;

typedef struct ikrmem_Scratch {
    usize size;
    usize used;
    unsigned char *base;
} ikrmem_Scratch;

IKR_MEM_PUBLIC void ikrmem_arena_init(ikrmem_Arena *arena, unsigned char *addr, usize size);
IKR_MEM_PUBLIC void ikrmem_scratch_init(ikrmem_Scratch *scratch, unsigned char *addr, usize size);
IKR_MEM_PUBLIC ikrmem_ByteSlice ikrmem_arena_push(ikrmem_Arena *arena, usize size, usize align);
IKR_MEM_PUBLIC ikrmem_ByteSlice ikrmem_scratch_push(ikrmem_Scratch *scratch, usize size, usize align);

#define _PUSH(allocator, size, align) _Generic((allocator), \
    ikrmem_Arena*: ikrmem_arena_push,\
    ikrmem_Scratch*: ikrmem_scratch_push\
    )(allocator, size, align)

#define IKR_MEM_NEW(allocator, type) ((type*) (_PUSH(allocator, sizeof(type), alignof(type))).ptr)
#define IKR_MEM_NEW_ITEMS(allocator, type, count) ((type*) (_PUSH(allocator, (count) * sizeof(type), alignof(type)).ptr))
#define IKR_MEM_NEW_STRING(allocator, count) ((char*)(_PUSH(allocator, (count), alignof(char)).ptr))

#endif

// ============================== Implementation ============================== //
#ifdef IKR_MEM_IMPLEMENTAION

IKR_MEM_INTERNAL inline b32 is_pow2(uintptr v) { return (v & (v - 1)) == 0; }

IKR_MEM_INTERNAL uintptr align_uintpr_forward(uintptr p, usize align) {
    IKR_ASSERT(is_pow2((uintptr)align));
    uintptr new_p = p;
    uintptr rem = new_p & ((uintptr)(align) - 1);
    if (rem != 0) new_p += ((uintptr)(align) - rem);
    return new_p;
}

IKR_MEM_INTERNAL void* align_rawptr_forward(void *p, usize align) {
    return (void*)(align_uintpr_forward((uintptr)p, align));
}

// TODO: allignment
IKR_MEM_PUBLIC void ikrmem_arena_init(ikrmem_Arena *arena, unsigned char *addr, usize size) {
    arena->base = addr;
    arena->size = size;
    arena->used = 0;
}

IKR_MEM_PUBLIC ikrmem_ByteSlice ikrmem_arena_push(ikrmem_Arena *arena, usize size, usize align) {
    ikrmem_ByteSlice data = {0};
    u8* last_base = arena->base + arena->used;
    u8* ptr = (u8*)(align_rawptr_forward(last_base, align));
    usize size_needed = size + (usize)(ptr - last_base);

    if ((arena->used + size_needed) <= arena->size) {
        data.ptr = ptr;
        data.len = size;
        arena->used += size_needed;
    }
    return data;
}

IKR_MEM_PUBLIC void ikrmem_scratch_init(ikrmem_Scratch *scratch, unsigned char *addr, usize size) {
    scratch->base = addr;
    scratch->size = size;
    scratch->used = 0;
}

IKR_MEM_PUBLIC ikrmem_ByteSlice ikrmem_scratch_push(ikrmem_Scratch *scratch, usize size, usize align) {
    ikrmem_ByteSlice data = {0};
    u8* last_base = scratch->base + scratch->used;
    u8* ptr = (u8*)(align_rawptr_forward(last_base, align));
    usize size_needed = size + (usize)(ptr - last_base);

    if ((scratch->used + size_needed) <= scratch->size) {
        data.ptr = ptr;
        data.len = size;
        scratch->used += size_needed;
    } else if (size_needed < scratch->size) {
        data.ptr = align_rawptr_forward(scratch->base, align);
        data.len = size;
        scratch->used = size_needed;
    } else {
        IKR_ASSERT_M(0, "size needed is bigger than scratch buffer size");
    }

    return data;
}

#endif
