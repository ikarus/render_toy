// ======================================================================================
// win32.h - v0.0.1 - [short description]
// Author: Ngoc Nguyen
// [long description]
// ======================================================================================

#ifndef WIN32_H
#define WIN32_H

#define DWORD_HI(word) ((word) >> 32)
#define DWORD_LO(word) ((word) & 0x000000FFFFFFFF)
#define SYS_EXIT ExitProcess

#ifndef USE_LOG
#define USE_LOG
#define os_debug_print win32_debug_print
#endif

#ifndef SYS_EXIT
#define SYS_EXIT ExitProcess
#endif

typedef struct Win32State {
    // mem used for platform is total_size - app_mem_size
    void *mem;
    usize total_size;
    void *app_mem;
    u64   app_mem_size;

    HCURSOR cursor;
    b32 show_cursor, is_fullscreen;
    WINDOWPLACEMENT win_pos;
} Win32State;

b32 win32_std_handles_init();
void* win32_valloc(void* base_addr, usize size, u32 alloc_type, u32 protect_flag);

OS_DEBUG_PRINT_PROC(win32_debug_print);
OS_READ_FILE_PROC(win32_read_file);
OS_READ_FILE_PROC(win32_read_asset_file);
OS_WRITE_FILE_PROC(win32_write_file);

#endif
