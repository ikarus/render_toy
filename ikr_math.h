#ifndef IKR_MATH_H
#define IKR_MATH_H

#ifdef __cplusplus
#define IKRM_PUBLIC extern "C"
#else
#define IKRM_PUBLIC extern
#endif

#define IKRM_INTERNAL static

typedef long                    i32;
typedef unsigned long           b32;
typedef float                   f32;
typedef double                  f64;

typedef union {
    struct { f32 x, y; };
    f32 v[2];
} v2f32;

typedef union {
    struct { f32 x, y, z; };
    f32 xy[2];
    f32 v[3];
} v3f32;

#define ikrm_f32x2(x, y) (v2f32){ (x), (y) }
#define ikrm_f32x3(x, y, z) (v3f32){ (x), (y), (z) }

#define V2(type, x, y) (v2##type) { (x), (y) }
#define V3(type, x, y) (v3##type) { (x), (y), (z) }

#define V2_ALL(type, x) (v2##type) { (x), (x) }
#define V3_ALL(type, x) (v3##type) { (x), (x), (x) }

#define V2_IS_ZERO(v) (((v).x == 0) && ((v).y == 0))
#define V3_IS_ZERO(v) (((v).x == 0) && ((v).y == 0) && ((v).z == 0))

#ifdef IKRM_SHORTHAND

#define v2(x, y) (v2f32){ (x), (y) }
#define v3(x, y, z) (v3f32){ (x), (y), (z) }
#define lerp ikrm_lerp_f32

#define add_v2       ikrm_add_v2f32
#define sub_v2       ikrm_sub_v2f32
#define add_eq_v2    ikrm_add_eq_v2f32
#define sub_eq_v2    ikrm_sub_eq_v2f32
#define mule_v2      ikrm_mule_v2f32
#define dive_v2      ikrm_dive_v2f32
#define mule_eq_v2   ikrm_mule_eq_v2f32
#define dive_eq_v2   ikrm_dive_eq_v2f32
#define muls_v2      ikrm_muls_v2f32
#define divs_v2      ikrm_divs_v2f32
#define muls_eq_v2   ikrm_muls_eq_v2f32
#define divs_eq_v2   ikrm_divs_eq_v2f32
#define dot_v2       ikrm_dot_prod_v2f32
#define eq_v2        ikrm_eq_v2f32
#define lerp_v2      ikrm_lerp_v2f32

#define add_v3       ikrm_add_v3f32
#define sub_v3       ikrm_sub_v3f32
#define add_eq_v3    ikrm_add_eq_v3f32
#define sub_eq_v3    ikrm_sub_eq_v3f32
#define mule_v3      ikrm_mule_v3f32
#define dive_v3      ikrm_dive_v3f32
#define mule_eq_v3   ikrm_mule_eq_v3f32
#define dive_eq_v3   ikrm_dive_eq_v3f32
#define muls_v3      ikrm_muls_v3f32
#define divs_v3      ikrm_divs_v3f32
#define muls_eq_v3   ikrm_muls_eq_v3f32
#define divs_eq_v3   ikrm_divs_eq_v3f32
#define dot_v3       ikrm_dot_prod_v3f32
#define eq_v3        ikrm_eq_v3f32
#define lerp_v3      ikrm_lerp_v3f32
#endif

// ============================== common math ============================== //
IKRM_PUBLIC inline f32       ikrm_lerp_f32(f32 a, f32 b, f32 t);


// ============================== vector math ============================== //
IKRM_PUBLIC inline v2f32     ikrm_add_v2f32(v2f32 v0, v2f32 v1);
IKRM_PUBLIC inline v2f32     ikrm_sub_v2f32(v2f32 v0, v2f32 v1);
IKRM_PUBLIC inline void      ikrm_add_eq_v2f32(v2f32 *v0, v2f32 v1);
IKRM_PUBLIC inline void      ikrm_sub_eq_v2f32(v2f32 *v0, v2f32 v1);
IKRM_PUBLIC inline v2f32     ikrm_mule_v2f32(v2f32 v0, v2f32 v1);
IKRM_PUBLIC inline v2f32     ikrm_dive_v2f32(v2f32 v0, v2f32 v1);
IKRM_PUBLIC inline void      ikrm_mule_eq_v2f32(v2f32 *v0, v2f32 v1);
IKRM_PUBLIC inline void      ikrm_dive_eq_v2f32(v2f32 *v0, v2f32 v1);
IKRM_PUBLIC inline v2f32     ikrm_muls_v2f32(v2f32 v, f32 s);
IKRM_PUBLIC inline v2f32     ikrm_divs_v2f32(v2f32 v, f32 s);
IKRM_PUBLIC inline void      ikrm_muls_eq_v2f32(v2f32 *v, f32 s);
IKRM_PUBLIC inline void      ikrm_divs_eq_v2f32(v2f32 *v, f32 s);
IKRM_PUBLIC inline f32       ikrm_dot_prod_v2f32(v2f32 v0, v2f32 v1);
IKRM_PUBLIC inline b32       ikrm_eq_v2f32(v2f32 v0, v2f32 v1);
IKRM_PUBLIC inline v2f32     ikrm_lerp_v2f32(v2f32 a, v2f32 b, f32 t);


IKRM_PUBLIC inline v3f32     ikrm_add_v3f32(v3f32 v0, v3f32 v1);
IKRM_PUBLIC inline v3f32     ikrm_sub_v3f32(v3f32 v0, v3f32 v1);
IKRM_PUBLIC inline void      ikrm_add_eq_v3f32(v3f32 *v0, v3f32 v1);
IKRM_PUBLIC inline void      ikrm_sub_eq_v3f32(v3f32 *v0, v3f32 v1);
IKRM_PUBLIC inline v3f32     ikrm_mule_v3f32(v3f32 v0, v3f32 v1);
IKRM_PUBLIC inline v3f32     ikrm_dive_v3f32(v3f32 v0, v3f32 v1);
IKRM_PUBLIC inline void      ikrm_mule_eq_v3f32(v3f32 *v0, v3f32 v1);
IKRM_PUBLIC inline void      ikrm_dive_eq_v3f32(v3f32 *v0, v3f32 v1);
IKRM_PUBLIC inline v3f32     ikrm_muls_v3f32(v3f32 v, f32 s);
IKRM_PUBLIC inline v3f32     ikrm_divs_v3f32(v3f32 v, f32 s);
IKRM_PUBLIC inline void      ikrm_muls_eq_v3f32(v3f32 *v, f32 s);
IKRM_PUBLIC inline void      ikrm_divs_eq_v3f32(v3f32 *v, f32 s);
IKRM_PUBLIC inline f32       ikrm_dot_prod_v3f32(v3f32 v0, v3f32 v1);
IKRM_PUBLIC inline b32       ikrm_eq_v3f32(v3f32 v0, v3f32 v1);
IKRM_PUBLIC inline v3f32     ikrm_lerp_v3f32(v3f32 a, v3f32 b, f32 t);

#endif

#ifdef IKR_MATH_IMPLEMENTATION

// ============================== common math ============================== //
IKRM_PUBLIC inline f32 ikrm_lerp_f32(f32 a, f32 b, f32 t) { return t * a + (1 - t) * b; }


// ============================== vector math ============================== //

// ============================== v2 ============================== //
IKRM_PUBLIC inline v2f32 ikrm_add_v2f32(v2f32 v0, v2f32 v1) { return ikrm_f32x2(v0.x + v1.x, v0.y + v1.y); }

IKRM_PUBLIC inline v2f32 ikrm_sub_v2f32(v2f32 v0, v2f32 v1) { return ikrm_f32x2(v0.x - v1.x, v0.y - v1.y); }

IKRM_PUBLIC inline void ikrm_add_eq_v2f32(v2f32 *v0, v2f32 v1) { v0->x += v1.x; v0->y += v1.y; }

IKRM_PUBLIC inline void ikrm_sub_eq_v2f32(v2f32 *v0, v2f32 v1) { v0->x -= v1.x; v0->y -= v1.y; }

IKRM_PUBLIC inline v2f32 ikrm_mule_v2f32(v2f32 v0, v2f32 v1) { return ikrm_f32x2(v0.x * v1.x, v0.y * v1.y); }

IKRM_PUBLIC inline v2f32 ikrm_dive_v2f32(v2f32 v0, v2f32 v1) { return ikrm_f32x2(v0.x / v1.x, v0.y / v1.y); }

IKRM_PUBLIC inline void ikrm_mule_eq_v2f32(v2f32 *v0, v2f32 v1) { v0->x *= v1.x; v0->y *= v1.y; }

IKRM_PUBLIC inline void ikrm_dive_eq_v2f32(v2f32 *v0, v2f32 v1) { v0->x /= v1.x; v0->y /= v1.y; }

IKRM_PUBLIC inline v2f32 ikrm_muls_v2f32(v2f32 v, f32 s) { return ikrm_f32x2(v.x * s, v.y * s); }

IKRM_PUBLIC inline v2f32 ikrm_divs_v2f32(v2f32 v, f32 s) { return ikrm_f32x2(v.x / s, v.y / s); }

IKRM_PUBLIC inline void ikrm_muls_eq_v2f32(v2f32 *v, f32 s) { v->x *= s; v->y *= s; }

IKRM_PUBLIC inline void ikrm_divs_eq_v2f32(v2f32 *v, f32 s) { v->x /= s; v->y /= s; }

IKRM_PUBLIC inline f32 ikrm_dot_prod_v2f32(v2f32 v0, v2f32 v1) { return v0.x * v1.x + v0.y * v1.y; }

IKRM_PUBLIC inline b32 ikrm_eq_v2f32(v2f32 v0, v2f32 v1) { return v0.x == v1.x && v0.y == v1.y; }

IKRM_PUBLIC inline v2f32 ikrm_lerp_v2f32(v2f32 a, v2f32 b, f32 t) { return ikrm_f32x2(ikrm_lerp_f32(a.x, b.x, t), ikrm_lerp_f32(a.y, b.y, t)); }


// ============================== v3 ============================== //
IKRM_PUBLIC inline v3f32 ikrm_add_v3f32(v3f32 v0, v3f32 v1) { return ikrm_f32x3(v0.x + v1.x, v0.y + v1.y, v0.z + v1.z); }

IKRM_PUBLIC inline v3f32 ikrm_sub_v3f32(v3f32 v0, v3f32 v1) { return ikrm_f32x3(v0.x - v1.x, v0.y - v1.y, v0.z - v1.z); }

IKRM_PUBLIC inline void ikrm_add_eq_v3f32(v3f32 *v0, v3f32 v1) { v0->x += v1.x; v0->y += v1.y; v0->z += v1.z; }

IKRM_PUBLIC inline void ikrm_sub_eq_v3f32(v3f32 *v0, v3f32 v1) { v0->x -= v1.x; v0->y -= v1.y; v0->z -= v1.z; }

IKRM_PUBLIC inline v3f32 ikrm_mule_v3f32(v3f32 v0, v3f32 v1) { return ikrm_f32x3(v0.x * v1.x, v0.y * v1.y, v0.z * v1.z); }

IKRM_PUBLIC inline v3f32 ikrm_dive_v3f32(v3f32 v0, v3f32 v1) { return ikrm_f32x3(v0.x / v1.x, v0.y / v1.y, v0.z / v1.z); }

IKRM_PUBLIC inline void ikrm_mule_eq_v3f32(v3f32 *v0, v3f32 v1) { v0->x *= v1.x; v0->y *= v1.y; v0->z *= v1.z; }

IKRM_PUBLIC inline void ikrm_dive_eq_v3f32(v3f32 *v0, v3f32 v1) { v0->x /= v1.x; v0->y /= v1.y; v0->z /= v1.z; }

IKRM_PUBLIC inline v3f32 ikrm_muls_v3f32(v3f32 v, f32 s) { return ikrm_f32x3(v.x * s, v.y * s, v.z * s); }

IKRM_PUBLIC inline v3f32 ikrm_divs_v3f32(v3f32 v, f32 s) { return ikrm_f32x3(v.x / s, v.y / s, v.z / s); }

IKRM_PUBLIC inline void ikrm_muls_eq_v3f32(v3f32 *v, f32 s) { v->x *= s; v->y *= s; v->z *= s; }

IKRM_PUBLIC inline void ikrm_divs_eq_v3f32(v3f32 *v, f32 s) { v->x /= s; v->y /= s; v->z /= s; }

IKRM_PUBLIC inline f32 ikrm_dot_prod_v3f32(v3f32 v0, v3f32 v1) { return v0.x * v1.x + v0.y * v1.y + v0.z * v1.z; }

IKRM_PUBLIC inline b32 ikrm_eq_v3f32(v3f32 v0, v3f32 v1) { return v0.x == v1.x && v0.y == v1.y && v0.z == v1.z; }

IKRM_PUBLIC inline v3f32 ikrm_lerp_v3f32(v3f32 a, v3f32 b, f32 t) { return ikrm_f32x3(ikrm_lerp_f32(a.x, b.x, t), ikrm_lerp_f32(a.y, b.y, t), ikrm_lerp_f32(a.z, b.z, t)); }

#endif

