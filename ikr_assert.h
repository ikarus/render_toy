// ======================================================================================
// ikr_assert.h - v0.0.1 - single header assert
// Author: Ngoc Nguyen
// ======================================================================================

#ifndef IKR_ASSERT_H
#define IKR_ASSERT_H

#ifdef __cplusplus
#define IKR_PUBLIC extern "C"
#else
#define IKR_PUBLIC extern
#endif
#define IKR_INTERNAL static

#ifdef IKR_NO_ASSERT

#define IKR_ASSERT(cond) ((void)(0))
#define IKR_ASSERT_M(cond, msg) ((void)(0))

#else

#define IKR_ASSERT(cond) ikr_assert((cond), __LINE__, __func__, __FILE__, 0)
#define IKR_ASSERT_M(cond, msg) ikr_assert((cond), __LINE__, __func__, __FILE__, (msg))

IKR_PUBLIC void ikr_assert(_Bool cond, int line, char *func, char *file, char *msg);

#endif
#endif

#ifdef IKR_ASSERT_IMPLEMENTATION

// Provide your own printf function by defining IKR_ASSERT_PRINT.
#ifndef IKR_ASSERT_PRINT

#if _WIN32
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
#include <stdarg.h>
#include "stb_sprintf.h"

#ifdef USE_CONSOLE
IKR_INTERNAL HANDLE __STDERR = INVALID_HANDLE_VALUE;
#endif

IKR_INTERNAL void ikr_debug_print(const char *fmt, ...) {
    char debug_str[1024];
    va_list args;
    va_start(args, fmt);
    int len = stbsp_vsnprintf(debug_str, 1024, fmt, args);
    va_end(args);
    OutputDebugString(debug_str);

#ifdef USE_CONSOLE
    DWORD written;
    if (__STDERR == INVALID_HANDLE_VALUE) {
        __STDERR = GetStdHandle(STD_ERROR_HANDLE);
    }
    if (__STDERR != INVALID_HANDLE_VALUE) WriteConsoleA(__STDERR, debug_str, len, &written, 0);
#endif
}
#define IKR_ASSERT_PRINT ikr_debug_print

#elif __linux__
#include <stdin.h>
#define IKR_ASSERT_PRINT printf

#elif __APPLE__
#include <stdin.h>
#define IKR_ASSERT_PRINT printf
#endif

#endif

#ifndef IKR_TRAP
#define IKR_TRAP ikr_trap

// NOTE: this causes a null pointer deref, win32 debuggers catch this pretty much always, don't know
//       about linux and apple. Technically the correct way to do this on win32 is to call RaiseException,
//       but like ..., no.
IKR_INTERNAL void ikr_trap() {
    int* dst = 0;
    *dst = 0;
}
#endif

IKR_INTERNAL char* trim_left_at_char(char *str, unsigned char c) {

    char* cur = str;
    char* res = str;

    while (*cur) {
        if (*cur == c) {
            res = cur;
        }
        cur++;
    }

    return res;
}

IKR_INTERNAL char* ikr_try_shorten_path(char *filepath) {
    char *short_path = 0;
    short_path = trim_left_at_char(filepath, '/');
    if (short_path != filepath) {
        short_path++;
    } else {
        short_path = trim_left_at_char(filepath, '\\');
        if (short_path != filepath) short_path++;
    }

    return short_path;
}

IKR_PUBLIC void ikr_assert(bool cond, int line, char *func, char *file, char *msg) {
    if (!cond) {
        if (msg) {
            IKR_ASSERT_PRINT("%s | %s | %d: Assertion failed, message: %s\n", ikr_try_shorten_path(file), func, line, msg);
        } else {
            IKR_ASSERT_PRINT("%s | %s | %d: Assertion failed\n", ikr_try_shorten_path(file), func, line);
        }
        IKR_TRAP();
    }
}


#endif
